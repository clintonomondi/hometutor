<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{

    protected $fillable = ['user_id', 'category_id', 'title', 'body'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}

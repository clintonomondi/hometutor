<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{

    protected $addHttpCookie = true;


    protected $except = [
        'get_c2bConfirmation',
        'get_c2KConfrmation'
    ];
}

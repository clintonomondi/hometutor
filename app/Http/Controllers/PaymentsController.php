<?php

namespace App\Http\Controllers;

use App\Payment;
use App\TutorProfile;
use App\TutionRequest;
use Illuminate\Http\Request;
use Knox\MPESA\Facades\MPESA;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PaymentsController extends Controller
{

    public function view_payments(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tutorprofiles = TutorProfile::where('PhoneNumber', 'LIKE', "%$keyword%")
                ->orWhere('Amount', 'LIKE', "%$keyword%")
                ->orWhere('TransactionDate', 'LIKE', "%$keyword%")
                ->orWhere('MpesaReceiptNumber', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $payments = Payment::latest()->paginate($perPage);
        }

        return view('admin.view_payments', compact('payments'));
    }

    public function showIndex($id)
    {
        $tutionrequest = TutionRequest::findOrFail($id);
        return view('moderator.test', compact('tutionrequest'));
    }

    public function registerURL()
    {
        $mpesa = MPESA::registerC2bUrl();
    }

    public function sumulateC2B($phone, $amount, $account = '', $description = 'No Description')
    {

        $phone = Input::get('PartyA');
        $amount = Input::get('Amount');
        $mpesa =  MPESA::stkPush($phone, $amount, $account, $description);
    }

    public function c2KConfrmation(Request $request)
    {
        $response = $request->all();
        $result_code = $response['Body']['stkCallback']['ResultCode'];

        if ($result_code == '0') {
            $collection = collect($response['Body']['stkCallback']['CallbackMetadata']['Item']);
            $mpesa_receipt_number = $collection->where('Name', 'MpesaReceiptNumber')->first();
            $transaction_date = $collection->where('Name', 'TransactionDate')->first();
            $phone_number = $collection->where('Name', 'PhoneNumber')->first();
            $amount = $collection->where('Name', 'Amount')->first();
        }

        // Log::info($amount["Value"]);
        // Log::info($transaction_date["Value"]);
        // Log::info($phone_number["Value"]);
        // Log::info($mpesa_receipt_number["Value"]);

        $transactions = new Payment();
        $transactions->Amount = $amount["Value"];
        $transactions->TransactionDate = $transaction_date["Value"];
        $transactions->PhoneNumber = $phone_number["Value"];
        $transactions->MpesaReceiptNumber =  $mpesa_receipt_number["Value"];

        $transactions->save();
        $message = ["ResultCode" => 0, "ResultDesc" => "Success"];
        // return response()->json($message);
        return back()->withInput($message);
    }

    public function c2bConfirmation(Request $request)
    {

        $response = json_decode($request->getContent(), true);
        $stk_callback =  $response['Body']['stkCallback'];
        $result_code = $stk_callback['ResultCode'];
        $result_desc = $stk_callback['ResultDesc'];

        $callback_metadata = $stk_callback['CallbackMetadata']['Item'];
        $transaction = [];

        foreach ($callback_metadata as $data) {
            $transaction[$data['Name']] = array_key_exists('Value', $data) ? $data['Value'] : '';
        }

        $amount  = isset($transaction["Amount"]["Value"]);
        Log::info($amount);
        $transaction_date = $transaction['TransactionDate'];
        $phone_number = $transaction['PhoneNumber'];
        $mpesa_receipt_number = $transaction['MpesaReceiptNumber'];
        Log::info($mpesa_receipt_number);

        $transactions = new Payment();
        $transactions->Amount = $amount;
        $transactions->TransactionDate = $transaction_date;
        $transactions->PhoneNumber = $phone_number;
        $transactions->MpesaReceiptNumber =  $mpesa_receipt_number;

        $transactions->save();
        $message = ["ResultCode" => 0, "ResultDesc" => "Success"];
        // return response()->json($message);
        return back()->withInput($message);
    }
}

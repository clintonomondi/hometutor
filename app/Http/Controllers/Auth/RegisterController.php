<?php

namespace App\Http\Controllers\Auth;

use Mail;
use App\User;
use App\Referal;
use App\VerifyUser;
use App\TutorProfile;
use App\Mail\VerifyMail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{


    use RegistersUsers;
    protected $redirectTo = '/home';

    public function redirectTo()
    {

          // Check user role
        //   switch ($role) {
        //       case 'Manager':
        //               return '/dashboard';
        //           break;
        //       case 'Employee':
        //               return '/projects';
        //           break;
        //       default:
        //               return '/login';
        //           break;
        //   }

        $id = Auth::user()->id;
        $user_id = User::find($id)->first();
        $tutor_id = $user_id->tutorprofile->user_id;

        $learner = TutorProfile::where('user_id', $tutor_id)->first();
        $status = $learner->status;


        if (Auth::user()->hasRole('Administrator')) {
            return '/home';
        } else if (Auth::user()->hasRole('Tutor') && !$status == 1) {
            return '/tutorProfile/tutor-profiles/create';
        } else if (Auth::user()->hasRole('Tutor') && $status == 1) {
            return '/tutor';
        } else if (Auth::user()->hasRole('Learner')) {
            return '/learnerProfile/learner-profiles/create';

        } else {
            return '/home';
        }
    }




    public function __construct()
    {
        $this->middleware('guest');
    }


    protected function validator(array $data)
    {


        if ($data["role"] == "Learner") {
            return Validator::make($data, [
                'learner_name' => 'required|string|max:255',
                'learner_phone_number' => 'required|string|max:255',
                'learner_location' => 'required|string|max:255',
                'learner_email' => 'required|string|email|max:255|unique:users,email',
                'password' => 'required|string|min:6|confirmed',
                'password_confirmation' => 'required ',

            ]);
        } else if ($data["role"] == "Tutor") {
            return Validator::make($data, [
                'name' => 'required|string|max:255',
                'tutor_location' => 'required|string|max:255',
                'phone_number' => 'required|string|max:255',
                'tutor_email' => 'required|string|email|max:255|unique:users,email',
                'password' => 'required|string|min:6|confirmed',
                'password_confirmation' => 'required ',
            ]);
        }
    }

    protected function create(array $data)
    {

        if ($data["role"] == "Tutor") {
            $user = User::create([
                'name' => $data['name'],
                'phone_number' => $data['phone_number'],
                'location' => $data['tutor_location'],
                'email' => $data['tutor_email'],
                'password' => bcrypt($data['password_confirmation']),
            ]);
            $user->assignRole('Tutor');
        } else if ($data["role"] == "Learner") {

            $user = User::create([
                'name' => $data['learner_name'],
                'email' => $data['learner_email'],
                'location' => $data['learner_location'],
                'phone_number' => $data['learner_phone_number'],
                'password' => bcrypt($data['password_confirmation']),
            ]);
            $user->assignRole('Learner');
        }
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);

        Mail::to($user->email)->send(new VerifyMail($user));
        return $user;
    }

    public function getLearner()
    {
        return view('auth.learner_register');

    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if ($verifyUser) {
            if (!$verifyUser->verified) {
                $verifyUser->verified = 1;
                $verifyUser->save();

                $status = "Your e-mail is verified. You can now login.";
            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect('/login')->with('status', $status);
    }



    protected function registered(Request $request, $user)
    {
        do {
            $token = $this->getToken(7, rand(10, 10000));
        } while (Referal::where('referal_code', $token)->count() > 0);
        $referal = new Referal;
        $referal->user_id = $user->id;
        $referal->referal_code = $token;
        $referal->status = 'Active';
        $referal->save();

        $referer_code = $request->input('referal_code');
        if ($referer_code) {
            $ref = Referal::where('referal_code', $referer_code)->first();
            if ($ref) {
                $used = new UsedReferal;
                $used->referal_id = $ref->id;
                $used->user_id = $user->id;
                $used->save();
            }
        }
    }

    private function getToken($length, $seed)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "0123456789";
        mt_srand($seed);

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[mt_rand(0, strlen($codeAlphabet) - 1)];
        }
        return $token;
    }
}

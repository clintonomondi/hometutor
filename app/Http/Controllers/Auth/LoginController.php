<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\TutorProfile;
use App\LearnerProfile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectTo = '/logins';

    public function redirectTo()
    {

        $id = Auth::user()->id;
        $user_id = User::findOrFail($id)->id;
        $tutor = TutorProfile::where('user_id', $user_id)->first();
        $learner = LearnerProfile::where('user_id', $user_id)->first();

        $t = $tutor->status;
        $l = $learner->status;

        if (Auth::user()->hasRole('Administrator')) {
            return '/home';
        } else if (Auth::user()->hasRole('Tutor') && $t == 0) {
            return '/tutorProfile/tutor-profiles/create';
        } else if (Auth::user()->hasRole('Tutor') && $t == 1) {
            return '/tutor';
        } else if (Auth::user()->hasRole('Learner') && $l == 0) {
            return '/learnerProfile/learner-profiles/create';
        } else if (Auth::user()->hasRole('Learner') && $l == 1) {
            return '/learner';
        }
    }


    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
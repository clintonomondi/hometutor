<?php

namespace App\Http\Controllers\TutorProfile;

use App\Subject;
use App\TutorProfile;

use App\Traits\UploadTrait;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TutorProfilesController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tutorprofiles = TutorProfile::where('area_of_specialization', 'LIKE', "%$keyword%")
                ->orWhere('academic_lavel', 'LIKE', "%$keyword%")
                ->orWhere('certification', 'LIKE', "%$keyword%")
                ->orWhere('cert_upload', 'LIKE', "%$keyword%")
                ->orWhere('years_of_experience', 'LIKE', "%$keyword%")
                ->orWhere('subjects_of_specialization', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tutorprofiles = TutorProfile::latest()->paginate($perPage);
        }

        return view('tutorProfile.tutor-profiles.index', compact('tutorprofiles'));
    }


    public function create()
    {
        $subjects = Subject::pluck( 'subject_name', 'subject_code');
        return view('tutorProfile.tutor-profiles.create', compact('subjects'));
    }

    public function postTutorProfile(Request $request)
    {
        $this->validate($request, [
			'area_of_specialization' => 'required',
			'academic_lavel' => 'required',
			'certification' => 'required',
			'cert_upload' => 'required',
			'years_of_experience' => 'required',
			'subjects_of_specialization' => 'required'
		]);
        $requestData = $request->all();

        $requestData['user_id'] = Auth::user()->id;
        $requestData['status'] =  1;

        TutorProfile::create($requestData);

        return redirect('/tutor')->with('flash_message', 'TutorProfile added!');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
			'area_of_specialization' => 'required',
			'academic_level' => 'required',
			'certification' => 'required',
			'cert_upload' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
			'years_of_experience' => 'required',
			'subjects_of_specialization' => 'required'
        ]);
        // dd('here');
        $requestData = $request->all();
        if ($request->has('cert_upload')) {
            // Get image file
            $image = $request->file('cert_upload');
            // Make a image name based on user name and current timestamp
            $name = Str::slug($request->input('certification')).'_'.time();
            // Define folder path
            $folder = '/uploads/images/';
            // Make a file path where image will be stored [ folder path + file name + file extension]
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $this->uploadOne($image, $folder, 'public', $name);
            // Set user profile image path in database to filePath
            $requestData['cert_upload'] = $filePath;
        }
 dd('here');

        $requestData['user_id'] = Auth::user()->id;
        $requestData['status'] =  1;

        TutorProfile::create($requestData);

        return redirect('/tutor')->with('flash_message', 'TutorProfile added!');
    }


    public function show($id)
    {
        $tutorprofile = TutorProfile::findOrFail($id);

        return view('tutorProfile.tutor-profiles.show', compact('tutorprofile'));
    }

    public function edit($id)
    {
        $tutorprofile = TutorProfile::findOrFail($id);

        return view('tutorProfile.tutor-profiles.edit', compact('tutorprofile'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'area_of_specialization' => 'required',
			'academic_lavel' => 'required',
			'certification' => 'required',
			'cert_upload' => 'required',
			'years_of_experience' => 'required',
			'subjects_of_specialization' => 'required',
			'user_id' => 'required'
		]);
        $requestData = $request->all();

        $tutorprofile = TutorProfile::findOrFail($id);
        $tutorprofile->update($requestData);

        return redirect('tutorProfile/tutor-profiles')->with('flash_message', 'TutorProfile updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TutorProfile::destroy($id);

        return redirect('tutorProfile/tutor-profiles')->with('flash_message', 'TutorProfile deleted!');
    }
}

<?php

namespace App\Http\Controllers;

use App\UsedReferal;
use Illuminate\Http\Request;

class UsedReferalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsedReferal  $usedReferal
     * @return \Illuminate\Http\Response
     */
    public function show(UsedReferal $usedReferal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsedReferal  $usedReferal
     * @return \Illuminate\Http\Response
     */
    public function edit(UsedReferal $usedReferal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsedReferal  $usedReferal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsedReferal $usedReferal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsedReferal  $usedReferal
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsedReferal $usedReferal)
    {
        //
    }
}

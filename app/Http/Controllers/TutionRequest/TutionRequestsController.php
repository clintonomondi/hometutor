<?php

namespace App\Http\Controllers\TutionRequest;

use App\Http\Requests;
use App\TutionRequest;

use Illuminate\Http\Request;
use willvincent\Rateable\Rating;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TutionRequestsController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tutionrequests = TutionRequest::where('start_tuition_date', 'LIKE', "%$keyword%")
                ->orWhere('end_tuition_date', 'LIKE', "%$keyword%")
                ->orWhere('subject_category', 'LIKE', "%$keyword%")
                ->orWhere('select_subject', 'LIKE', "%$keyword%")
                ->orWhere('total_tuition_cost', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tutionrequests = TutionRequest::latest()->paginate($perPage);
        }

        return view('learner.index', compact('tutionrequests'));
    }


    public function create()
    {
        return view('tutionrequest.tution-requests.create');
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'start_tuition_date' => 'required',
            'end_tuition_date' => 'required',
            'subject_category' => 'required',
            'select_subject' => 'required',
            'total_tuition_cost' => 'required'

        ]);


        // dd($request->user_id );
        $requestData = $request->all();
        $requestData['status'] = "pending";
        $requestData['user_id'] = Auth::user()->id;
        TutionRequest::create($requestData);

        return redirect('tutionrequest/tution-requests')->with('flash_message', 'TutionRequest added!');
    }



    public function show($id)
    {
        $tutionrequest = TutionRequest::findOrFail($id);

        return view('tutionrequest.tution-requests.show', compact('tutionrequest'));
    }


    public function edit($id)
    {
        $tutionrequest = TutionRequest::findOrFail($id);

        return view('tutionrequest.tution-requests.edit', compact('tutionrequest'));
    }


    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'start_tuition_date' => 'required',
            'end_tuition_date' => 'required',
            'subject_category' => 'required',
            'select_subject' => 'required',
            'total_tuition_cost' => 'required'
        ]);

        $requestData = $request->all();
        // dd(  $requestData );
        $tutionrequest = TutionRequest::findOrFail($id);
        $requestData['user_id'] = Auth::user()->id;
        $requestData['status'] = "Assigned To Tutor";
        // dd(  $requestData );
        $tutionrequest->update($requestData);

        return redirect('tutionrequest/tution-requests')->with('flash_message', 'TutionRequest updated!');
    }


    public function destroy($id)
    {
        TutionRequest::destroy($id);

        return redirect('tutionrequest/tution-requests')->with('flash_message', 'TutionRequest deleted!');
    }


    public function postStar(Request $request, TutionRequest $tutionrequest)
    {
        $rating = new Rating;
        $rating->user_id = Auth::id();
        $rating->rating = $request->input('star');
        $tutionrequest->ratings()->save($rating);
        return redirect()->back();
    }

    public function postPost(Request $request)
    {
        request()->validate(['rate' => 'required']);
        $tutionrequest = TutionRequest::find($request->id);
        $rating = new Rating;
        $rating->rating = $request->rate;
        $rating->user_id = auth()->user()->id;
        $tutionrequest->ratings()->save($rating);
        return redirect()->route("posts");
    }
}

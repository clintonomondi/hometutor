<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function view_tutor_profile()
    {
        return view('tutor_profile');
    }

    public function view_learner_profile()
    {
        return view('learner_profile');
    }
}

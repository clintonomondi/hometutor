<?php

namespace App\Http\Controllers;

use App\Order;
use App\Subject;
use App\Category;
use Illuminate\Http\Request;

class OrderController extends Controller
{


    public function create_order()
    {
        $categories = Category::all()->pluck('group_name', 'group')->toArray();
        $subjects = Subject::all()->pluck('subject_name', 'id')->toArray();

        return view('learner.place_an_order', compact(['subjects', 'categories']));
    }


    public function post_an_order(Request $request)
    {
        //
    }



    public function update_an_order(Request $request, Order $order)
    {
        //
    }
}

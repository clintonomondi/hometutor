<?php

namespace App\Http\Controllers;

use App\Dispute;
use MPESA;
use App\Tutor;
use App\Moderator;
use App\TutorProfile;
use App\TutionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ModeratorController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        // $user_id =

        if (!empty($keyword)) {
            $tutionrequests = TutionRequest::where('start_tuition_date', 'LIKE', "%$keyword%")
                ->orWhere('end_tuition_date', 'LIKE', "%$keyword%")
                ->orWhere('subject_category', 'LIKE', "%$keyword%")
                ->orWhere('select_subject', 'LIKE', "%$keyword%")
                ->orWhere('total_tuition_cost', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tutionrequests = TutionRequest::latest()->paginate($perPage);
        }

        return view('moderator.index', compact('tutionrequests'));
    }

    public function assignTutor(Request $request, $id)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tutionrequests = TutionRequest::where('start_tuition_date', 'LIKE', "%$keyword%")
                ->orWhere('end_tuition_date', 'LIKE', "%$keyword%")
                ->orWhere('subject_category', 'LIKE', "%$keyword%")
                ->orWhere('select_subject', 'LIKE', "%$keyword%")
                ->orWhere('total_tuition_cost', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tutionrequests = TutionRequest::latest()->paginate($perPage);
        }
        $tutors = Tutor::all()->pluck('name', 'id')->toArray();
        $tutionrequest = TutionRequest::findOrFail($id);
        return view('moderator.assign_tutor', compact(['tutionrequest', 'tutionrequests', 'tutors']));
    }


    public function showRequest($id)
    {
        // $user_id = Auth::user()->id;

        // $tutor_profile = TutorProfile::with('user')-> where([['status', '=', '1'], ['user_id', '=', $user_id]])->get();
        // // dd($tutor_profile);
        // foreach ($tutor_profile as $item) {
        //     dd($item->user);
        // }

        $tutionrequest = TutionRequest::findOrFail($id);

        return view('moderator.show_request', compact('tutionrequest'));
    }


    public function listDisputes()
    {

        $perPage = 25;


        $disputes = Dispute::latest()->paginate($perPage);


        return view('moderator.list_disputes', compact('disputes'));
    }


    public function show(Moderator $moderator)
    {
        $tutionrequest = TutionRequest::findOrFail($moderator);

        return view('moderator.tution-requests.show', compact('tutionrequest'));
    }
}

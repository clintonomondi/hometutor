<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\TutionRequest;

class DashboardController extends Controller
{



    public function getTutors()
    {

        $tutor = Role::where('name', 'Tutor')->first();
        $tutor_id =  $tutor->id;

        $tutors  = User::whereHas('roles', function($query) use($tutor_id) {   $query->where('role_id', $tutor_id); })->with('roles')->get();
        return view('admin.dashboard.get_members', compact('tutors'));
    }

    public function getLearners()
    {

        $learner = Role::where('name', 'Learner')->first();
        $learner_id =  $learner->id;

        $learners = User::whereHas('roles', function($query) use($learner_id) {  $query->where('role_id', $learner_id); })->with('roles')->get();
        return view('admin.dashboard.get_learners', compact('learners'));
    }

    public function getTutionRequests(Request $request)
    {

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tutionrequests = TutionRequest::where('start_tuition_date', 'LIKE', "%$keyword%")
                ->orWhere('end_tuition_date', 'LIKE', "%$keyword%")
                ->orWhere('subject_category', 'LIKE', "%$keyword%")
                ->orWhere('select_subject', 'LIKE', "%$keyword%")
                ->orWhere('total_tuition_cost', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tutionrequests = TutionRequest::latest()->paginate($perPage);
        }

        return view('admin.dashboard.get_tution_requests', compact('tutionrequests'));
    }



}

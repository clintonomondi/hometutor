<?php

namespace App\Http\Controllers;

use App\Tutor;
use Illuminate\Http\Request;

class TutorController extends Controller
{

    public function index()
    {
        return view('tutor.index');
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show(Tutor $tutor)
    {
        //
    }


    public function edit(Tutor $tutor)
    {
        //
    }


    public function update(Request $request, Tutor $tutor)
    {
        //
    }


    public function destroy(Tutor $tutor)
    {
        //
    }
}

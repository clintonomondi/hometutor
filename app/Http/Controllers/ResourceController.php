<?php

namespace App\Http\Controllers;

use App\Category;
use App\Resource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResourceController extends Controller
{

    public function index()
    {
        return view('resource.index');
    }


    public function show_resource(Resource $resource)
    {
        return view('resource.show_resource');
    }


    public function create_resource()
    {
        $categories = Category::pluck('group_name', 'id')->toArray();
        return view('resource.create_resource', compact('categories'));
    }

    public function post_resource(Request $request)
    {

        $requestData = new Resource();
        $requestData->category = $request->category;
        $requestData->title = $request->title;
        $requestData->body = $request->body;
        $requestData->user_id =  Auth::user()->id;

        // dd($requestData);
        $requestData->save();

        return view('resource.index');
    }


    public function dropzoneStore(Request $request)
    {
        $image = $request->file('file');

        $imageName = time().'.'.$image->extension();
        $image->move(public_path('images'),$imageName);

        return response()->json(['success'=>$imageName]);
    }

}

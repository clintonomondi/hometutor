<?php

namespace App\Http\Controllers;

use App\Dispute;
use App\TutionRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class LearnerController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $tutionrequests = TutionRequest::where('start_tuition_date', 'LIKE', "%$keyword%")
                ->orWhere('end_tuition_date', 'LIKE', "%$keyword%")
                ->orWhere('subject_category', 'LIKE', "%$keyword%")
                ->orWhere('select_subject', 'LIKE', "%$keyword%")
                ->orWhere('total_tuition_cost', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tutionrequests = TutionRequest::oldest()->paginate($perPage);
        }

        return view('learner.index', compact('tutionrequests'));
    }

    public function review_completed_request($id)
    {
        $tutionrequest = TutionRequest::findOrFail($id);
        // dd($tutionrequest);
        return view('learner.complete_request_view', compact('tutionrequest'));
    }

    public function dispute_completed_request($id)
    {
        $learnerprofile = TutionRequest::findOrFail($id);
        return view('learner.complete_request_view', compact('learnerprofile'));
    }

    public function post_dispute(Request $request)
    {
        $requestData = new Dispute();
        $requestData->tution_id = $name = Input::get('tution_id');
        $requestData->title = $request->title;
        $requestData->body = $request->body;
        $requestData->user_id =  Auth::user()->id;


        $requestData->save();

        return view('resource.index');
    }
}

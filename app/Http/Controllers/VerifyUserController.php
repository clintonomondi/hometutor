<?php

namespace App\Http\Controllers;

use App\verifyUser;
use Illuminate\Http\Request;

class VerifyUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\verifyUser  $verifyUser
     * @return \Illuminate\Http\Response
     */
    public function show(verifyUser $verifyUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\verifyUser  $verifyUser
     * @return \Illuminate\Http\Response
     */
    public function edit(verifyUser $verifyUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\verifyUser  $verifyUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, verifyUser $verifyUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\verifyUser  $verifyUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(verifyUser $verifyUser)
    {
        //
    }
}

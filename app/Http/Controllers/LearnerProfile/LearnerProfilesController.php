<?php

namespace App\Http\Controllers\LearnerProfile;

use App\Category;
use App\Education;

use App\Http\Requests;
use App\LearnerProfile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LearnerProfilesController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $learnerprofiles = LearnerProfile::where('user_id', 'LIKE', "%$keyword%")
                ->orWhere('gender', 'LIKE', "%$keyword%")
                ->orWhere('education_system', 'LIKE', "%$keyword%")
                ->orWhere('learning_level', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $learnerprofiles = LearnerProfile::latest()->paginate($perPage);
        }

        return view('learnerProfile.learner-profiles.index', compact('learnerprofiles'));
    }


    public function create()
    {   $educations = Education::pluck( 'education_name');
        $education_level = Education::pluck( 'group');
        return view('learnerProfile.learner-profiles.create', compact(['educations','education_level']));
    }


    public function postLearnerProfile(Request $request)
    {
        $this->validate($request, [
			'gender' => 'required',
			'education_system' => 'required',
			'learning_level' => 'required'
		]);
        $requestData = $request->all();
        $requestData['user_id'] = Auth::user()->id;
        $requestData['status'] = 1;
        LearnerProfile::create($requestData);

        return redirect('/learner')->with('flash_message', 'LearnerProfile added!');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'gender' => 'required',
			'education_system' => 'required',
			'learning_level' => 'required'
		]);
        $requestData = $request->all();
        $requestData['user_id'] = Auth::user()->id;
        $requestData['status'] = 1;
        // dd($requestData);
        LearnerProfile::create($requestData);

        return redirect('/learner')->with('flash_message', 'LearnerProfile added!');
    }


    public function show($id)
    {
        $learnerprofile = LearnerProfile::findOrFail($id);

        return view('learnerProfile.learner-profiles.show', compact('learnerprofile'));
    }

    public function edit($id)
    {
        $learnerprofile = LearnerProfile::findOrFail($id);

        return view('learnerProfile.learner-profiles.edit', compact('learnerprofile'));
    }


    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'user_id' => 'required',
			'gender' => 'required',
			'education_system' => 'required',
			'learning_level' => 'required'
		]);
        $requestData = $request->all();

        $learnerprofile = LearnerProfile::findOrFail($id);
        $learnerprofile->update($requestData);

        return redirect('learnerProfile/learner-profiles')->with('flash_message', 'LearnerProfile updated!');
    }


    public function destroy($id)
    {
        LearnerProfile::destroy($id);

        return redirect('learnerProfile/learner-profiles')->with('flash_message', 'LearnerProfile deleted!');
    }
}

<?php

namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{

    protected $fillable = [
        'subject_code', 'subject_code', 'category_id'
    ];

    public function categories()
    {
        return $this->belongsTo(Category::class);
    }
}

<?php

namespace App;


use App\Resource;
use App\TutorProfile;
use App\TutionRequest;
use App\LearnerProfile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRoles;


    protected $fillable = [
        'name', 'email', 'password', 'phone_number', 'location'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser', 'user_id');
    }

    public function tutorprofile()
    {
        return $this->hasOne(TutorProfile::class);
    }

    public function learnerprofile()
    {
        return $this->hasOne(LearnerProfile::class);
    }


    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function resources()
    {
        return $this->hasMany(Resource::class);
    }

    public function tutionrequest()
    {
        return $this->belongsTo(TutionRequest::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referal extends Model
{
    protected $table = 'referals';


    protected $primaryKey = 'id';


    protected $fillable = ['user_id', 'status', 'referal_code'];

    public function referal()
    {
        return $this->belongsTo('App\User');
    }
}

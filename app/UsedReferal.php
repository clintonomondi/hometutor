<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsedReferal extends Model
{
    protected $table = 'used_referals';


    protected $primaryKey = 'id';

    public function used_referal()
    {
        return $this->belongsTo('App\User');
    }

    protected $fillable = ['referal_id', 'user_id'];

    public function referal()
    {
        return $this->belongsTo(Referal::class);
    }
}

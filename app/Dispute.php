<?php

namespace App;

use App\TutionRequest;
use Illuminate\Database\Eloquent\Model;

class Dispute extends Model
{
    protected $fillable = ['title', 'body', 'user_id', 'tution_id'];

    public function tuitionrequest()
    {
        return $this->belongsTo(TutionRequest::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class VerifyUser extends Model
{

    protected $table = 'verify_users';


    protected $primaryKey = 'id';


    protected $fillable = ['user_id', 'token', 'verified'];





    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}

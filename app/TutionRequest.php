<?php

namespace App;

use App\User;
use App\Tutor;
use App\Dispute;
use Illuminate\Database\Eloquent\Model;

class TutionRequest extends Model
{


    protected $table = 'tution_requests';


    protected $primaryKey = 'id';


    protected $fillable = ['start_tuition_date', 'end_tuition_date', 'user_id', 'tutor_id', 'status', 'subject_category', 'select_subject', 'total_tuition_cost'];


    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }

    public function tutor()
    {
        return $this->belongsTo(Tutor::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function dispute()
    {
        return $this->hasOne(Dispute::class);
    }
}

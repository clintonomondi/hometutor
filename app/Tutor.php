<?php

namespace App;

use App\TutionRequest;
use willvincent\Rateable\Rateable;
use Illuminate\Database\Eloquent\Model;

class Tutor extends Model
{



    use Rateable;


    public function tutionrequests()
    {
        return $this->hasMany(TutionRequest::class);
    }
}

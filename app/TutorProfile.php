<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TutorProfile extends Model
{

    protected $table = 'tutor_profiles';


    protected $primaryKey = 'id';


    protected $fillable = [
        'area_of_specialization',
        'user_id','status',
        'academic_level',
        'certification',
        'cert_upload',
        'years_of_experience',
        'subjects_of_specialization'
    ];

    public function getImageAttribute()
    {
       return $this->cert_upload;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}

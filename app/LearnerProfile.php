<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class LearnerProfile extends Model
{

    protected $table = 'learner_profiles';


    protected $primaryKey = 'id';

    protected $fillable = ['user_id', 'gender','status', 'education_system', 'learning_level'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}

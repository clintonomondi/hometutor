<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/main.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/font-awesome/4.7.0/css/font-awesome.min.css') }}" />
      <title>Login - One on One HomeTutor</title>
   </head>
   <body>
      <section class="material-half-bg">
         <div class="cover"></div>
      </section>
      <section class="login-content">
         <div class="logo">
            <h1> HomeTutor</h1>
         </div>
         <div class="login-box register-box">
            <form class="login-form" method="POST" action="{{ route('register') }}">
               @csrf
               <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i> REGISTER AS A LEARNER</h3>
               <div class="form-group">
                  <label for="learner_name" class="control-label">{{ __('Name') }}</label>
                  <input class="form-control @error('learner_name') is-invalid @enderror" id="name" type="learner_name" placeholder="Name" name="learner_name" value="{{ old('learner_name') }}" required autocomplete="learner_name" autofocus> @error('tutor_name')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span> @enderror
               </div>
               <div class="form-group">
                  <label for="learner_location" class="control-label">{{ __('Location') }}</label>
                  <select class="form-control @error('learner_location') is-invalid @enderror" name="learner_location" value="{{ old('learner_location') }}" >
                     <option value="Nairobi">Nairobi</option>
                     <option value="Kisumu">Kisumu</option>
                     <option value="Mombasa">Mombasa</option>
                     <option value="Nakuru">Nakuru</option>
                  </select>
               </div>
               <div class="form-group">
                  <label for="learner_phone_number" class="control-label">{{ __('Phone Number') }}</label>
                  <input class="form-control @error('learner_phone_number') is-invalid @enderror" id="learner_phone_number" type="text" placeholder="Learner Phone Number" name="learner_phone_number" value="{{ old('learner_phone_number') }}" required autocomplete="learner_phone_number" autofocus> @error('learner_phone_number')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span> @enderror
               </div>
               <div class="form-group">
                  <label for="learner_email" class="control-label">{{ __('E-Mail Address') }}</label>
                  <input class="form-control @error('learner_email') is-invalid @enderror" id="learner_email" type="email" placeholder="Email" name="learner_email" value="{{ old('tutor_email') }}" required autocomplete="email" autofocus> @error('tutor_email')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span> @enderror
               </div>
               <div class="form-group">
                  <label for="password" class="control-label">{{ __('Password') }}</label>
                  <input id="password" type="password"  name="password" class="form-control @error('password') is-invalid @enderror"
                     required autocomplete="current-password">
                  @error('password')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span> @enderror
               </div>
               <div class="form-group">
                  <label for="password-confirm" class="control-label">{{ __('Confirm Password') }}</label>
                  <input id="password-confirm" type="password" class="form-control @error('password-confirm') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password"> @error('password-confirm')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span> @enderror
                  <input class="invisible" name="role" value="Learner">
               </div>
               <div class="form-group btn-container">
                  <button class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>REGISTER HERE</button>
               </div>
            </form>
         </div>
      </section>
      <!-- Essential javascripts for application to work-->
      <script src="{{ asset('backend/js/jquery-3.2.1.min.js') }}"></script>
      <script src="{{ asset('backend/js/popper.min.js') }}"></script>
      <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('backend/js/main.js') }}"></script>
      <script src="{{ asset('backend/js/plugins/pace.min.js') }}"></script>
   </body>
</html>

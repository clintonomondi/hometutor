<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/main.css') }}" />
      <link rel="stylesheet" type="text/css" href="{{ asset('backend/css/font-awesome/4.7.0/css/font-awesome.min.css') }}" />
      <title>Login - One on One HomeTutor</title>
   </head>
   <body>
      <section class="material-half-bg">
         <div class="cover"></div>
      </section>
      <section class="login-content">
         <div class="logo">
            <h1>HomeTutor</h1>
         </div>
         <div class="login-box register-box">
            <form class="login-form" method="POST" action="{{ route('register') }}">
               @csrf
               <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i> REGISTER AS A TUTOR</h3>
               <div class="form-group">
                  <label for="tutor_name" class="control-label">{{ __('Name') }}</label>
                  <input class="form-control @error('tutor_name') is-invalid @enderror" id="name" type="name" placeholder="Name" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus> @error('tutor_name')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span> @enderror
               </div>
               <div class="form-group">
                  <label for="location" class="control-label">{{ __('Location') }}</label>
                  <select class="form-control @error('location') is-invalid @enderror" name="tutor_location" value="{{ old('location') }}" >
                     <option value="Nairobi">Nairobi</option>
                     <option value="Kisumu">Kisumu</option>
                     <option value="Mombasa">Mombasa</option>
                     <option value="Nakuru">Nakuru</option>
                  </select>
               </div>
               <div class="form-group">
                  <label for="tutor_phone_number" class="control-label">{{ __('Phone Number') }}</label>
                  <input class="form-control @error('tutor_phone_number') is-invalid @enderror" id="phone_number" type="tutor_phone_number" placeholder="Phone Number" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="tutor_phone_number" autofocus> @error('tutor_phone_number')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span> @enderror
               </div>
               <div class="form-group">
                  <label for="tutor_email" class="control-label">{{ __('E-Mail Address') }}</label>
                  <input class="form-control @error('tutor_email') is-invalid @enderror" id="tutor_email" type="email" placeholder="Email" name="tutor_email" value="{{ old('tutor_email') }}" required autocomplete="email" autofocus> @error('tutor_email')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span> @enderror
               </div>
               <div class="form-group">
                  <label for="password" class="control-label">{{ __('Password') }}</label>
                  <input id="password" type="password"  name="password" class="form-control @error('password') is-invalid @enderror"
                     required autocomplete="current-password">
                  @error('password')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span> @enderror
               </div>
               <div class="form-group">
                  <label for="password-confirm" class="control-label">{{ __('Confirm Password') }}</label>
                  <input id="password-confirm" type="password" class="form-control @error('password-confirm') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password"> @error('password-confirm')
                  <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                  </span> @enderror
               </div>
               <input class="invisible" name="role" value="Tutor">
               <div class="form-group btn-container">
                  <button class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>REGISTER HERE</button>
               </div>
            </form>
         </div>
      </section>
      <!-- Essential javascripts for application to work-->
      <script src="{{ asset('backend/js/jquery-3.2.1.min.js') }}"></script>
      <script src="{{ asset('backend/js/popper.min.js') }}"></script>
      <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('backend/js/main.js') }}"></script>
      <script src="{{ asset('backend/js/plugins/pace.min.js') }}"></script>
   </body>
</html>

<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
   <div class="app-sidebar__user">
      <div>
         <p class="app-sidebar__user-name">{{ Auth::user()->name }} </p>
         <p class="app-sidebar__user-designation">Super  Administrator</p>
      </div>
   </div>
   <ul class="app-menu">

        <li>
            <a class="app-menu__item active" href="{{ route("getTutorMermbers") }}"><i class="app-menu__icon fa fa-dashboard"></i>
                <span class="app-menu__label">Dashboard</span>
            </a>
        </li>

       @unless (Auth::user()->hasRole('Tutor') || Auth::user()->hasRole('Administrator') || Auth::user()->hasRole('Moderator'))
      <li class="treeview">
         <a class="app-menu__item" href="#"  data-toggle="treeview"><i class="app-menu__icon fa fa-pencil"></i>
         <span class="app-menu__label">Tutors</span>
         <i class="treeview-indicator fa fa-angle-right"></i>
         </a>
         <ul class="treeview-menu">
            <li>
               <a class="treeview-item" href="/tutorProfile/tutor-profiles/create"><i class="app-menu__icon fa fa-pencil"></i>Update Profile</a>
            </li>
            <li>
               <a class="treeview-item" href="/moderator_show_request/1" rel="noopener noreferrer"><i class="app-menu__icon fa fa-pencil"></i>View Tution Requests</a>
            </li>
            <li>
               <a class="treeview-item" href="#"  rel="noopener noreferrer"><i class="app-menu__icon fa fa-pencil"></i>View Tution Payments</a>
            </li>
         </ul>
      </li>
      @endunless
      @unless (Auth::user()->hasRole('Learner') || Auth::user()->hasRole('Administrator') || Auth::user()->hasRole('Moderator'))
      <li class="treeview">
         <a class="app-menu__item"  href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
         <span class="app-menu__label">Learners</span>
         <i class="treeview-indicator fa fa-angle-right"></i>
         </a>
         <ul class="treeview-menu">
            <li>
               <a class="treeview-item" href="/learnerProfile/learner-profiles/create"><i class="app-menu__icon fa fa-pencil"></i>Update Profile</a>
            </li>
            <li>
               <a class="treeview-item" href="#"  rel="noopener noreferrer"><i class="app-menu__icon fa fa-pencil"></i>Make Tution Requests</a>
            </li>
            <li>
               <a class="treeview-item" href="#"  rel="noopener noreferrer"><i class="app-menu__icon fa fa-pencil"></i>View Tution Requests</a>
            </li>
         </ul>
      </li>
      @endunless
      @unless (Auth::user()->hasRole('Administrator') || Auth::user()->hasRole('Moderator'))
      <li class="treeview">
         <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-users"></i>
         <span class="app-menu__label">Moderators</span>
         <i class="treeview-indicator fa fa-angle-right"></i>
         </a>
         <ul class="treeview-menu">
            <li>
                <a class="treeview-item" href="{{ route('moderatorIndex') }}" rel="noopener noreferrer"><i class="icon fa fa-pencil"></i>Listed Requests</a>
             </li>
            <li>
                <a class="treeview-item" href="{{ route('listDisputes')}}"  rel="noopener noreferrer"><i class="icon fa fa-pencil"></i>
                    Listed Disputes</a>
             </li>

            <li>
                <a class="treeview-item" href="{{ route('moderatorIndex') }}" rel="noopener noreferrer"><i class="icon fa fa-pencil"></i>Listed Tutors</a>
             </li>
             <li>
                <a class="treeview-item" href="{{ route('moderatorIndex') }}" rel="noopener noreferrer"><i class="icon fa fa-pencil"></i>Listed Learners</a>
             </li>
            <li>
               <a class="treeview-item" href="/home"  rel="noopener noreferrer"><i class="icon fa fa-pencil"></i>View Tution Payments</a>
            </li>

         </ul>
      </li>
      @endunless
      @unless (Auth::user()->hasRole('Moderator') || Auth::user()->hasRole('Administrator') || Auth::user()->hasRole('Moderator'))
      <li class="treeview">
         <a class="app-menu__item" href="#" data-toggle="treeview"><i class="app-menu__icon fa fa-pencil"></i>
         <span class="app-menu__label">Resources</span>
         <i class="treeview-indicator fa fa-angle-right"></i>
         </a>
         <ul class="treeview-menu">
            <li>
               <a class="treeview-item" href="{{ route('resourceIndex') }}"><i class="icon fa fa-circle-o"></i>View Resources</a>
            </li>
            <li>
               <a class="treeview-item" href="{{ route('createResource') }}"><i class="icon fa fa-circle-o"></i>Create Resource</a>
            </li>
         </ul>
      </li>
      @endunless
      <li>
         <a class="app-menu__item" href="#"><i class="app-menu__icon fa fa-cogs"></i>
         <span class="app-menu__label">Settings</span>
         </a>
      </li>
   </ul>
</aside>

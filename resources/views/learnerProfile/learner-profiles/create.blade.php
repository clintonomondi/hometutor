@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="page-header">
                 <h2 class="mb-3 line-head" id="typography">Update  Your Learner  Profile</h2>
                </div>
                <div class="row">
                <div class="col-lg-6">
                        <form method="POST" class="form-horizontal" action="{{ route('postLearnerProfile') }}">
                         @csrf
                        <div class="tile">
                                    <div class="form-group{{ $errors->has('gender') ? 'has-error' : ''}}">
                                        <label for="sel1">Select Gender:</label>
                                        <select class="form-control" id="sel1"  name="gender" required>
                                          <option>Select Gender:</option>
                                          <option>Male</option>
                                          <option>Female</option>
                                          <option>Other</option>
                                        </select>
                                      </div>

                                    <div class="form-group{{ $errors->has('education_system') ? 'has-error' : ''}}">
                                            {!! Form::label('education_system', 'Mode of Study', ['class' => 'control-label']) !!}
                                            {!! Form::select('education_system',$educations, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                            {!! $errors->first('education_system', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group{{ $errors->has('learning_level') ? 'has-error' : ''}}">
                                                {!! Form::label('learning_level', 'Learning Level', ['class' => 'control-label']) !!}
                                                {!! Form::select('learning_level', $education_level, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                                {!! $errors->first('learning_level', '<p class="help-block">:message</p>') !!}
                                    </div>
                         </div>
                         </div>

                       </div>
                             <div class="tile-footer">
                                   <button class="btn btn-primary" type="submit">Submit Updated Profile</button>
                                 </div>
                     </form>
              </div>
            </div>
          </div>
        </div>
    </div>


@endsection

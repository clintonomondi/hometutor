<div class="form-group{{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'control-label']) !!}
    {!! Form::text('user_id', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('gender') ? 'has-error' : ''}}">
    {!! Form::label('gender', 'Gender', ['class' => 'control-label']) !!}
    {!! Form::text('gender', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('education_system') ? 'has-error' : ''}}">
    {!! Form::label('education_system', 'Education System', ['class' => 'control-label']) !!}
    {!! Form::text('education_system', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('education_system', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('learning_level') ? 'has-error' : ''}}">
    {!! Form::label('learning_level', 'Learning Level', ['class' => 'control-label']) !!}
    {!! Form::text('learning_level', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('learning_level', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>

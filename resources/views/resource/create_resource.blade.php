@extends('admin.app') @section('title') Dashboard @endsection @section('content')


<div class="row">
    <div class="col-md-12">

        <div class="tile">
        </span>
        </br>

        <div class="bs-component">
            <div class="row">

                            <div class="col-md-12">
                                <div class="tile">
                                    <div class="page-header">
                                     <h2 class="mb-3 line-head" id="typography">Create a Resource</h2>
                                    </div>
                                    {{-- {!! Form::open(['url' => '/post_resource', 'class' => 'form-horizontal', 'files' => true]) !!} --}}
                                    <form method="POST" class="form-horizontal" action="{{ route('postResource') }}"  >
                                            @csrf
                                                         <div class="form-group{{ $errors->has('subject_category') ? 'has-error' : ''}}">
                                                            {!! Form::label('subject_category', 'Subject Category', ['class' => 'control-label']) !!}
                                                            {!! Form::select('category', $categories, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                                            {!! $errors->first('subject_category', '<p class="help-block">:message</p>') !!}
                                                        </div>

                                                        {{-- <div class="form-group">
                                                                {!! Form::Label('item', 'Item:') !!}
                                                                <select class="form-control" name="category">
                                                                  @foreach($categories as $item)
                                                                    <option> {{ $item->group_name }}</option>
                                                                  @endforeach
                                                                </select>
                                                         </div> --}}
                                                        <div class="form-group{{ $errors->has('title') ? 'has-error' : ''}}">
                                                            {!! Form::label('title', 'Title Resource', ['class' => 'control-label']) !!}
                                                            {!! Form::text('title', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                                            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                                                        </div>
                                                        <div class="form-group{{ $errors->has('body') ? 'has-error' : ''}}">
                                                            {!! Form::label('body', 'Body of Resource', ['class' => 'control-label']) !!}
                                                            {!! Form::textarea('body', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                                            {!! $errors->first('body', '<p class="help-block">:message</p>') !!}
                                                        </div>
                                                         {{-- <div class="form-group{{ $errors->has('cert_upload') ? 'has-error' : ''}}">
                                                                <div>
                                                                        <h3>Upload Multiple Image By Click On Box</h3>
                                                                </div>
                                                         </div> --}}
                                                       <button class="btn btn-primary" type="submit">Submit Updated Profile</button>

                                    </form>



                            </div>
                        </div>





        </div>


    </div>
</div>
</div>


@endsection




@extends('admin.app') @section('title') Dashboard @endsection @section('content')
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="page-header">
                <h2 class="mb-3 line-head" id="typography">View Tution Request </h2>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="tile">
                        <div class="card">
                            <div class="card-header"><b><a  href="{{ route('moderatorIndex') }}"><button class="btn btn-primary pull-left" type="button"><i class="fa fa-fw fa-lg fa-eye"></i>View Tution Request</button></a></b>
                                <b><a  href="{{ route('reviewCompletedRequests',$tutionrequest->id ) }}"><button class="btn btn-info pull-right" type="button"><i class="fa fa-fw fa-lg fa-eye"></i>Complete and Review</button></a></b>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <td><strong><b>Education System</b></strong> </td>
                                            <td>High School 8-4-4</td>
                                        </tr>

                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><strong><b>Subject Name</b></strong> </td>
                                            <td>{{ $tutionrequest->select_subject }}</td>

                                        </tr>
                                        <tr>
                                            <td><strong>Subject Category</strong> </td>
                                            <td>{{ $tutionrequest->subject_category }} </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Request Status</strong> </td>
                                            <td>{{ $tutionrequest->status }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Tution Cost</strong> </td>
                                            <td>KES {{ $tutionrequest->total_tuition_cost }}.00</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Payment Status </strong></td>
                                                <td>Paid</td>
                                         </tr>
                                        <tr>
                                            <td><strong>Start Tution Date </strong></td>
                                            <td>{{ $tutionrequest->start_tuition_date }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>End Tution Date </strong></td>
                                            <td>{{ $tutionrequest->end_tuition_date }}</td>
                                        </tr>


                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <br>
                        <div class="card">
                                <div class="card-header">
                                        <a  href="{{ route('viewTutorProfile')  }}"><button class="btn btn-secondary pull-left" type="button"><i class="fa fa-fw fa-lg fa-eye"></i>View Tutors Profile</button></a>

                                </div>
                                <div class="card-body">
                                    <strong> <p>  Learner Location </p></strong>
                                     {{-- <p>{{ $tutionrequest->tutor->name }}</p> --}}
                                </div>
                                <div class="card-footer">
                                    <strong>  <p>  Learner Location </p></strong>
                                     {{-- <p> {{ $tutionrequest->tutor->location }} </p> --}}
                                </div>
                            </div>
                            <br>
                            <div class="card">
                                <div class="card-header">
                                        <a  href="{{ route('viewLearnerProfile')  }}"> <button class="btn btn-secondary" type="button"><i class="fa fa-fw fa-lg fa-eye"></i>View Learners Profile</button></a>
                                </div>
                                <div class="card-body">
                                    <strong> <p>  Learner Name </p></strong>
                                    <p>{{ $tutionrequest->user->name }}</p>
                                </div>
                                <div class="card-footer">
                                    <strong>  <p>  Learner Location </p></strong>
                                    <p> {{ $tutionrequest->user->location }} </p>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-lg-5">
                <div class="tile alert  alert-success">
                        <div class="page-header alert  alert-success">
                                <div class="card-body ">

                                <div id="accordion alert-success">
                                        <div class="card">
                                          <div class="card-header" id="headingOne">
                                                <h2 class="mb-3 line-head" id="typography">
                                                        <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fa fa-fw fa-lg fa-eye"></i>Read Payment Instruction </button>
                                                  </h2>
                                          </div>
                                          <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                            <div class="card-body">
                                                    To Pay your bill (KES. KES 150.00) via MPESA.
                                                    Follow the Steps Below. Once you receive a successful reply from Mpesa. Click the complete button bellow.

                                                    Go to M-PESA on your phone
                                                    Select Pay Bill option
                                                    Enter Business no. 206206
                                                    Enter Account no. JQNAHMX
                                                    Enter the Amount. KES 150.00
                                                    Enter your M-PESA PIN and Send
                                                    You will receive a confirmation SMS from MPESA
                                            </div>
                                          </div>
                                        </div>
                                        <br>
                                        <div class="card ">
                                          <div class="card-header" id="headingTwo">
                                            <h5 class="mb-0">
                                              <button class="btn btn-primary" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="fa fa-fw fa-lg fa-money"></i>Make  Payment via M-PESA  </button>
                                            </h5>
                                          </div>
                                          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="card-body ">
                                                    <form class="form-horizontal" method="POST" action="{{ route('postSumulateC2B') }}">
                                                            @csrf

                                                        {{-- <div class="form-group">
                                                                <label class="control-label"><strong> PAYBILL</strong></label>
                                                                    <input class="form-control " type="datetime"  readonly value="603014" placeholder="Enter full name">
                                                                  </div> --}}
                                                                <div class="form-group">
                                                                        <label class="control-label">Tution Cost</label>
                                                                        <input class="form-control " type="datetime" name="Amount" readonly value="{{   $tutionrequest->total_tuition_cost }}" placeholder="Enter full name">
                                                                      </div>

                                                                          <div class="form-group">
                                                                                <label class="control-label">Phone number</label>
                                                                                <input class="form-control " type="text" name="PartyA" value="{{  Auth::user()->phone_number }}" readonly placeholder="Phone number">
                                                                            </div>
                                                            <button type="submit" class="btn btn-primary"><i class="fa fa-fw fa-lg fa-check-circle"></i>Make Payment</button>
                                                </form>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                </div>
                         </div>
            </div>
        </div>
    </div>
</div>
@endsection

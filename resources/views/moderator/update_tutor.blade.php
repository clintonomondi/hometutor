<div class="form-group{{ $errors->has('total_tuition_cost') ? 'has-error' : ''}}">
        {!! Form::label('Tutor', 'Select Tutor', ['class' => 'control-label']) !!}
        {!! Form::select('tutor_id',  $tutors, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
        {!! $errors->first('total_tuition_cost', '<p class="help-block">:message</p>') !!}
 </div>

 <div class="form-group">
        <label class="control-label">Start Tuition Date</label>
        <input class="form-control" type="datetime" name="start_tuition_date" readonly value="{{  old('start_tuition_date', $tutionrequest->start_tuition_date) }}" placeholder="Enter full name">
      </div>
      <div class="form-group">
            <label class="control-label">End Tuition Date</label>
            <input class="form-control" type="datetime" name="end_tuition_date" readonly value="{{  old('start_tuition_date', $tutionrequest->end_tuition_date) }}" placeholder="Enter full name">
          </div>
          <div class="form-group">
                <label class="control-label">End Tuition Date</label>
                <input class="form-control" type="text" name="select_subject" readonly value="{{  old('select_subject', $tutionrequest->select_subject) }}" placeholder="Enter full name">
              </div>
              <div class="form-group">
                    <label class="control-label">Subject Category</label>
                    <input class="form-control" type="text" name="subject_category" readonly value="{{  old('start_tuition_date', $tutionrequest->subject_category) }}" placeholder="Enter full name">
                  </div>
                  <div class="form-group">
                        <label class="control-label">Total Tuition Cost</label>
                        <input class="form-control" type="text" name="total_tuition_cost" readonly value="{{  old('total_tuition_cost', $tutionrequest->total_tuition_cost) }}" placeholder="Enter full name">
                      </div>

    <div class="form-group">
        {!! Form::submit($formMode === 'edit' ? 'Assign Tutor' : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>

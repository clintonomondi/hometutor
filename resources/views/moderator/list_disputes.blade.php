@extends('admin.app') @section('title') Dashboard @endsection @section('content')
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="page-header">
                <h2 class="mb-3 line-head" id="typography">Disputed Tuition Requests </h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="card">
                            <div class="card-header"><b>
                                <a href="#"><button class="btn btn-primary" type="button"><i class="fa fa-fw fa-lg fa-eye"></i>Disputed Tuition Requests</button></a></b>
                                {{-- <a href="#"><button class="btn btn-danger pull-right" data-toggle="modal" data-target="#exampleModal" type="button"><i class="fa fa-times-circle-o" aria-hidden="true"></i>Dispute Results</button></a></b> --}}
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <td><strong><b>#Id</b></strong> </td>
                                            <td><strong><b>Dispute Title</b></strong> </td>
                                            <td><strong>Dispute Body</strong></td>
                                            <td>Actions</td>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach($disputes as $item)
                                        <tr>
                                            <td>{{ $item->id }} </td>
                                            <td>{{ $item->title }} </td>
                                            <td>{{ $item->body  }} </td>
                                            <td>

                                                  <button class="btn btn-success btn-sm"  type="button"><i class="fa fa-times-circle-o" aria-hidden="true"></i>Resolve Dispute</button>

                                            </td>
                                        </tr>

                                        @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>

                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>



@endsection

@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">TutorProfile {{ $tutorprofile->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/tutorProfile/tutor-profiles') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/tutorProfile/tutor-profiles/' . $tutorprofile->id . '/edit') }}" title="Edit TutorProfile"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['tutorProfile/tutorprofiles', $tutorprofile->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete TutorProfile',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $tutorprofile->id }}</td>
                                    </tr>
                                    <tr><th> Area Of Specialization </th><td> {{ $tutorprofile->area_of_specialization }} </td></tr><tr><th> Academic Lavel </th><td> {{ $tutorprofile->academic_lavel }} </td></tr><tr><th> Certification </th><td> {{ $tutorprofile->certification }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<div class="form-group{{ $errors->has('area_of_specialization') ? 'has-error' : ''}}">
    {!! Form::label('area_of_specialization', 'Area Of Specialization', ['class' => 'control-label']) !!}
    {!! Form::text('area_of_specialization', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('area_of_specialization', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('academic_level') ? 'has-error' : ''}}">
    {!! Form::label('academic_level', 'Academic Lavel', ['class' => 'control-label']) !!}
    {!! Form::text('academic_level', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('academic_level', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('certification') ? 'has-error' : ''}}">
    {!! Form::label('certification', 'Certification', ['class' => 'control-label']) !!}
    {!! Form::text('certification', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('certification', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group{{ $errors->has('cert_upload') ? 'has-error' : ''}}">
    {!! Form::label('cert_upload', 'Cert Upload', ['class' => 'control-label']) !!}
    {!! Form::file('cert_upload', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('cert_upload', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('years_of_experience') ? 'has-error' : ''}}">
    {!! Form::label('years_of_experience', 'Years Of Experience', ['class' => 'control-label']) !!}
    {!! Form::text('years_of_experience', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('years_of_experience', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('subjects_of_specialization') ? 'has-error' : ''}}">
    {!! Form::label('subjects_of_specialization', 'Subjects Of Specialization', ['class' => 'control-label']) !!}
    {!! Form::text('subjects_of_specialization', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('subjects_of_specialization', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'control-label']) !!}
    {!! Form::number('user_id', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>

@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="page-header">
                 <h2 class="mb-3 line-head" id="typography">Update Your Tutor Profile</h2>
                </div>
                {!! Form::open(['url' => '/tutorProfile/tutor-profiles', 'class' => 'form-horizontal', 'files' => true]) !!}
                <div class="row">

                <div class="col-lg-6">

                        <div class="tile">
                                   <div class="form-group{{ $errors->has('area_of_specialization') ? 'has-error' : ''}}">
                                        {!! Form::label('area_of_specialization', 'Area Of Specialization', ['class' => 'control-label']) !!}
                                        {!! Form::text('area_of_specialization', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                        {!! $errors->first('area_of_specialization', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group{{ $errors->has('academic_level') ? 'has-error' : ''}}">
                                        {!! Form::label('academic_level', 'Academic Lavel', ['class' => 'control-label']) !!}
                                        {!! Form::text('academic_level', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                        {!! $errors->first('academic_lavel', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group{{ $errors->has('certification') ? 'has-error' : ''}}">
                                        {!! Form::label('certification', 'Certification', ['class' => 'control-label']) !!}
                                        {!! Form::text('certification', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                        {!! $errors->first('certification', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group{{ $errors->has('cert_upload') ? 'has-error' : ''}}">
                                            {!! Form::label('cert_upload', 'Certificate  Upload', ['class' => 'control-label']) !!}
                                            <input id="cert_upload" type="file" class="form-control" name="cert_upload">
                                            {{--  @if (auth()->user()->image)
                                                    <code>{{ auth()->user()->image }}</code>
                                            @endif  --}}

                                    </div>

                         </div>
                </div>
                <div class="col-lg-4 offset-lg-1">
                               <div class="tile">
                                    <div class="form-group{{ $errors->has('years_of_experience') ? 'has-error' : ''}}">
                                            {!! Form::label('years_of_experience', 'Years Of Experience', ['class' => 'control-label']) !!}
                                            {!! Form::text('years_of_experience', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                            {!! $errors->first('years_of_experience', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    {{-- <div class="form-group{{ $errors->has('subjects_of_specialization') ? 'has-error' : ''}}">
                                                {!! Form::label('subjects_of_specialization', 'Select Category', ['class' => 'control-label']) !!}
                                                {!! Form::select('subjects_of_specialization', $subjects, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                                {!! $errors->first('select_subject', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group{{ $errors->has('subjects_of_specialization') ? 'has-error' : ''}}">
                                        {!! Form::label('subjects_of_specialization', 'Select of Specialization', ['class' => 'control-label']) !!}
                                        {!! Form::select('subjects_of_specialization', $subjects, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                        {!! $errors->first('select_subject', '<p class="help-block">:message</p>') !!}
                            </div> --}}
                               </div>
                </div>

                    </div>
                    <div class="tile-footer">
                            <button class="btn btn-primary" type="submit">Submit Updated Profile</button>
                     </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
    </div>

    {{-- @section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script type="text/javascript">
      $('#provinces').on('change', function(e){
        console.log(e);
        var province_id = e.target.value;
        $.get('/json-regencies?province_id=' + province_id,function(data) {
          console.log(data);
          $('#regencies').empty();
          $('#regencies').append('<option value="0" disable="true" selected="true">=== Select Regencies ===</option>');

          $.each(data, function(index, regenciesObj){
            $('#regencies').append('<option value="'+ regenciesObj.id +'">'+ regenciesObj.name +'</option>');
          })
        });
      });

      $('#regencies').on('change', function(e){
        console.log(e);
        var regencies_id = e.target.value;
        $.get('/json-districts?regencies_id=' + regencies_id,function(data) {
          console.log(data);
          $('#districts').empty();
          $('#districts').append('<option value="0" disable="true" selected="true">=== Select Districts ===</option>');

          $.each(data, function(index, districtsObj){
            $('#districts').append('<option value="'+ districtsObj.id +'">'+ districtsObj.name +'</option>');
          })
        });
      });

    </script>

    @stop --}}
@endsection

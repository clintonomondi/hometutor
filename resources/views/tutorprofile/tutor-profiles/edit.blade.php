@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Edit TutorProfile #{{ $tutorprofile->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/tutorProfile/tutor-profiles') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::model($tutorprofile, [
                            'method' => 'PATCH',
                            'url' => ['/tutorProfile/tutor-profiles', $tutorprofile->id],
                            'class' => 'form-horizontal',
                            'files' => true
                        ]) !!}

                        @include ('tutorProfile.tutor-profiles.form', ['formMode' => 'edit'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

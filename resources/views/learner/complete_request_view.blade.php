@extends('admin.app') @section('title') Dashboard @endsection @section('content')
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="page-header">
                <h2 class="mb-3 line-head" id="typography">Review Tutor </h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="card">
                            <div class="card-header"><b>
                                <a href="{{ route('moderatorShowRequest',$tutionrequest->id ) }}"><button class="btn btn-primary" type="button"><i class="fa fa-fw fa-lg fa-eye"></i>View Tution Request</button></a></b>
                                <a href="#"><button class="btn btn-danger pull-right" data-toggle="modal" data-target="#exampleModal" type="button"><i class="fa fa-times-circle-o" aria-hidden="true"></i>Dispute Results</button></a></b>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <td><strong><b>Subject Name</b></strong> </td>
                                            <td>Learner Location</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><strong>Subject Category</strong> </td>
                                            <td>Learner Location </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Request Status</strong> </td>
                                            <td>Learner Location</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Tution Cost</strong> </td>
                                            <td>Learner Location</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Start Tution Date </strong></td>
                                            <td>Learner Location</td>
                                        </tr>
                                        <tr>
                                            <td><strong>End Tution Date </strong></td>
                                            <td>Learner Location</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tile-body">
                            <br>
                                <form>
                                  <div class="form-group">
                                    <textarea class="form-control" rows="4" placeholder="Comments For the Review " spellcheck="false"></textarea>
                                  </div>
                                    <h4>Star Rating</h4>
                                    <span class="fa fa-star checked" style="color: orange;"></span>
                                    <span class="fa fa-star checked" style="color: orange;"></span>
                                    <span class="fa fa-star checked" style="color: orange;"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <br><br>
                                  <div class="card-footer"><button class="btn btn-primary" type="button"><i class="fa fa-fw fa-lg fa-check-circle"></i>Complete  Review </button></div>
                                </form>
                              </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Dispute Tuition</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{ route('postDispute', $tutionrequest->id ) }}">
            @csrf
            <div class="form-group">
              <label for="recipient-name" class="col-form-label">Dispute Title:</label>
              <input type="text" name="title" class="form-control" id="recipient-name">
            </div>
            <div class="form-group">
              <label for="message-text" class="col-form-label">Dispute Message:</label>
              <textarea class="form-control" name="body" id="message-text" rows="12"></textarea>
            </div>
            <input class="invisible" name="tution_id" value="{{ $tutionrequest->id }}">
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Send message</button>
              </div>
          </form>
        </div>

      </div>
    </div>
  </div>


@endsection

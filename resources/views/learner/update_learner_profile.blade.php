@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')


        <div class="row">
            <div class="col-md-12">

              <div class="tile">
                    <div class="page-header">
                            <h2 class="mb-3 line-head" id="typography">Update Your Profile</h2>
                          </div>
                <div class="row">
                  <div class="col-lg-6">
                        {!! Form::open(['url' => '/tutorprofile/tutor-profiles', 'class' => 'form-horizontal', 'files' => true]) !!}
                 <div class="tile">


                            <div class="form-group{{ $errors->has('area_of_study') ? 'has-error' : ''}}">
                                {!! Form::label('area_of_study', 'Area Of Study', ['class' => 'control-label']) !!}
                                {!! Form::text('area_of_study', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                {!! $errors->first('area_of_study', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('academic_level') ? 'has-error' : ''}}">
                                {!! Form::label('academic_level', 'Academic Level', ['class' => 'control-label']) !!}
                                {!! Form::text('academic_level', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                {!! $errors->first('academic_level', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('certification') ? 'has-error' : ''}}">
                                {!! Form::label('certification', 'Certification', ['class' => 'control-label']) !!}
                                {!! Form::text('certification', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                {!! $errors->first('certification', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group{{ $errors->has('cert_uploads') ? 'has-error' : ''}}">
                                {!! Form::label('cert_uploads', 'Cert Uploads', ['class' => 'control-label']) !!}
                                {!! Form::text('cert_uploads', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                {!! $errors->first('cert_uploads', '<p class="help-block">:message</p>') !!}
                            </div>
                  </div>
                  </div>
                  <div class="col-lg-4 offset-lg-1">
                        <div class="tile">
                                <div class="form-group{{ $errors->has('year_of_experience') ? 'has-error' : ''}}">
                                        {!! Form::label('year_of_experience', 'Year Of Experience', ['class' => 'control-label']) !!}
                                        {!! Form::text('year_of_experience', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                        {!! $errors->first('year_of_experience', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group{{ $errors->has('subjects_of_specialization') ? 'has-error' : ''}}">
                                        {!! Form::label('subjects_of_specialization', 'Subjects Of Specialization', ['class' => 'control-label']) !!}
                                        {!! Form::text('subjects_of_specialization', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                        {!! $errors->first('subjects_of_specialization', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group{{ $errors->has('gender') ? 'has-error' : ''}}">
                                            {!! Form::label('gender', 'Gender', ['class' => 'control-label']) !!}
                                            {!! Form::text('gender', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                            {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
                                        </div>


                            {{--  <div class="form-group">  --}}
                                {{--  <label for="exampleSelect1">Subjects of Specialization</label>  --}}
                                          {{--  <div class="tile-body">  --}}
                                            {{--  <select class="form-control" name="subjects_of_specialization" id="demoSelect" multiple="">
                                              <optgroup label="Select Subjects">
                                                <option value="Mathematics">Mathematics</option>
                                                <option value="Biology">Biology</option>
                                                <option value="English">English</option>
                                                <option value="Kiswahili">Kiswahili</option>
                                                <option value="Physics">Physics</option>
                                                <option value="Geography">Geography</option>
                                                <option value="History">History</option>
                                                <option value="Business">Business</option>
                                              </optgroup>
                                            </select>  --}}
                                          {{--  </div>  --}}
                                        {{--  </div>  --}}
                      </div>
                    </div>
                </div>
                      <div class="tile-footer">
                            <button class="btn btn-primary" type="submit">Submit Updated Profile</button>
                          </div>
                {!! Form::close() !!}


              </div>
            </div>
          </div>


@endsection

@extends('admin.app') @section('title') Dashboard @endsection @section('content')
<div class="row">
    <div class="col-md-12">

        <div class="tile">
            <div class="page-header">
                <h2 class="mb-3 line-head" id="typography">View Tution Request </h2>
            </div>
            <div class="row">

                <div class="col-md-6">
                    <div class="tile">
                        <div class="card">

                            <div class="card-header"><b><button class="btn btn-primary" type="button"><i class="fa fa-fw fa-lg fa-eye"></i>View Tution Request</button></b>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                            <tr>
                                                    <td><strong><b>Subject Name</b></strong> </td>
                                                    <td>{{ $tutionrequest->select_subject }}</td>

                                                </tr>
                                        <tr>
                                            <td><strong><b>Subject Name</b></strong> </td>
                                            <td>{{ $tutionrequest->select_subject }}</td>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><strong>Subject Category</strong> </td>
                                            <td>{{ $tutionrequest->subject_category }} </td>
                                        </tr>
                                        <tr>
                                            <td><strong>Request Status</strong> </td>
                                            <td>{{ $tutionrequest->status  }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Tution Cost</strong> </td>
                                            <td>KES {{ $tutionrequest->total_tuition_cost }}.00</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Start Tution Date </strong></td>
                                            <td>{{ $tutionrequest->start_tuition_date  }}</td>
                                        </tr>
                                        <tr>
                                            <td><strong>End Tution Date </strong></td>
                                            <td>{{ $tutionrequest->end_tuition_date  }}</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                            <div class="card-footer"><button class="btn btn-primary" type="button"><i class="fa fa-fw fa-lg fa-check-circle"></i>Make Payment</button></div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-4   offset-lg-1">

                    <div class="tile">
                        <div class="page-header">

                            <div class="card">
                                <div class="card-header">
                                        <button class="btn btn-secondary" type="button"><i class="fa fa-fw fa-lg fa-user"></i>Assigned Tutor Profile</button>

                                </div>
                                <div class="card-body">
                                       <strong> <p>  Learner Location </p></strong>
                                    <p>{{ $tutionrequest->tutor->name }}</p>
                                </div>
                                <div class="card-footer">
                                      <strong>  <p>  Learner Location </p></strong>
                                  <p>  {{ $tutionrequest->tutor->location }} </p>
                                </div>
                            </div>
                            <br>

                            <div class="card">
                                    <div class="card-header">
                                            <button class="btn btn-secondary" type="button"><i class="fa fa-fw fa-lg fa-user"></i>Learner Details</button>
                                    </div>
                                    <div class="card-body">
                                            <strong> <p>  Learner Name </p></strong>
                                         <p>{{ $tutionrequest->user->name }}</p>
                                     </div>
                                     <div class="card-footer">
                                           <strong>  <p>  Learner Location </p></strong>
                                       <p>  {{ $tutionrequest->user->location }} </p>
                                     </div>
                                    </div>
                                </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
</div>

@endsection

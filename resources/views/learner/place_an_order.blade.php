@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')
<div class="row">
   <div class="col-md-12">
      <div class="tile">
        <div class="pull-right">
            <a class="btn btn-primary icon-btn" href=""><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>Back To Requests</a>
         </div>
         <div class="page-header ">
            <a class="btn btn-primary icon-btn" href=""><i class="fa fa-plus-square" aria-hidden="true"></i>Learner Tution Request</a>
            <h2 class="mb-3 line-head " id="typography">
            </h2>
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="tile">
                  <h3 class="tile-title">Subject Charges  per Hour</h3>
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Subject Category</th>
                           <th>Duration/Hourly</th>
                           <th>Total Cost</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>1</td>

                           <td>Otto</td>
                           <td>1 hour</td>
                           <td>@mdo</td>
                        </tr>
                        <tr>
                           <td>2</td>

                           <td>Thornton</td>
                           <td>2 hours</td>
                           <td>@fat</td>
                        </tr>
                        <tr>
                           <td>3</td>

                           <td>the Bird</td>
                           <td>3 hours</td>
                           <td>@twitter</td>
                        </tr>
                        <tr>
                           <td>4</td>

                           <td>the Bird</td>
                           <td>4 hours</td>
                           <td>@twitter</td>
                        </tr>

                     </tbody>
                  </table>
               </div>
            </div>
            <div class="col-lg-4   offset-lg-1">
               <div class="tile">
                  <div class="page-header">
                     <h2 class="mb-3 line-head" id="typography">Learner Tution Request </h2>
                     @if ($errors->any())
                     <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                     </ul>
                     @endif
                     {!! Form::open(['url' => '/tutionrequest/tution-requests', 'class' => 'form-horizontal', 'files' => true]) !!}
                     @include ('tutionrequest.tution-requests.form', ['formMode' => 'create'])
                     {!! Form::close() !!}
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Home</title>
      <meta charset="utf-8">
      <meta name="format-detection" content="telephone=no" />
      <link rel="icon" href="{{ asset('images/favicon.ico')  }}">
      <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />
      <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
      <script src="{{ asset('assets/js/jquery.js') }}"></script>
      <script src="{{ asset('assets/js/jquery-migrate-1.1.1.js') }}"></script>
      <script src="{{ asset('assets/js/jquery.easing.1.3.js') }}"></script>
      <script src="{{ asset('assets/js/script.js') }}"></script>
      <script src="{{ asset('assets/js/superfish.js') }}"></script>
      <script src="{{ asset('assets/js/jquery.equalheights.js') }}"></script>
      <script src="{{ asset('assets/js/jquery.mobilemenu.js') }}"></script>
      <script src="{{ asset('assets/js/tmStickUp.js') }}"></script>
      <script src="{{ asset('assets/js/jquery.ui.totop.js') }}"></script>
      <script>
         $(window).load(function(){
          $().UItoTop({ easingType: 'easeOutQuart' });
          $('#stuck_container').tmStickUp({});
         });
      </script>
      <!--[if lt IE 8]>
      <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
         <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
         </a>
      </div>
      <![endif]-->
      <!--[if lt IE 9]>
      <script src="{{ asset('assets/js/html5shiv.js')  }}"></script>
      <link rel="stylesheet" media="screen" href="{{ asset('css/ie.css') }}">
      <![endif]-->
   </head>
   <body class="page1" id="top">
      <!--==============================
         header
         =================================-->
      <header>
         <div class="container">
            <div class="row">
               <div class="grid_12 rel">
                  <h1>
                     <a href="index.html">v
                     HomeTutor
                     </a>
                  </h1>
               </div>
            </div>
         </div>
         <section id="stuck_container">
            <!--==============================
               Stuck menu
               =================================-->
            <div class="container">
               <div class="row">
                  <div class="grid_12 ">
                     <div class="navigation ">
                        <nav>
                           <ul class="sf-menu">
                              <li class="current"><a href="/">Home</a></li>
                              <li class="nav-item">
                                 <a class="nav-link page-scroll" href="{{ route('ourCurriculum') }}"> Our Curriculum</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link page-scroll active" href="/register">Become a Tutor</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link page-scroll" href="{{ route('getLearner') }}">Become a Learner</a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link active" href="/login"> Login </a>
                              </li>
                              <li class="nav-item">
                                 <a class="nav-link " href="#"> Contacts</a>
                              </li>
                           </ul>
                        </nav>
                        <div class="clear"></div>
                     </div>
                     <div class="clear"></div>
                  </div>
               </div>
            </div>
         </section>
      </header>
      <!--=====================
         Content
         ======================-->
      <section id="content">
         <article class="content_gray offset__1">
            <div class="container">
               <div class="row">
                  <div class="grid_12">
                     <h3>What We Offer</h3>
                  </div>
                  <div class="grid_6">
                     <div class="block-3">
                        <div class="count">1</div>
                        <div class="extra_wrapper">
                           <div class="text1"><a href="#">Cambridge IGCSE  Year 1 -12  </a></div>
                           The International General Certificate of Secondary Education (IGCSE) is an English language based examination which is recognized in the UK. We have over 42 local subjects’ experienced tutors available for you everywhere for home tutoring. Hire best & professional tutors for your private lessons..
                        </div>
                     </div>
                  </div>
                  <div class="grid_6">
                     <div class="block-3">
                        <div class="count">2</div>
                        <div class="extra_wrapper">
                           <div class="text1"><a href="#">Edexcel International GCSE Year 1-12 </a></div>
                           Edexcel International GCSEs are globally recognized qualifications with academic content and assessment designed specifically for international learners. We have over 25 local subjects’ experienced tutors available for you everywhere for home tutoring. Hire best & professional tutors for your private lessons.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="grid_12">
                     <h3></h3>
                  </div>
                  <div class="grid_6">
                     <div class="block-3">
                        <div class="count">3</div>
                        <div class="extra_wrapper">
                           <div class="text1"><a href="#">The International English Language Testing System (IELTS)
                              </a>
                           </div>
                           The International English Language Testing System, or IELTS is an international standardised test of English language proficiency for non-native English language speakers.  We have experienced tutors available for you everywhere for home tutoring. Hire best & professional tutors for your private lessons.
                        </div>
                     </div>
                  </div>
                  <div class="grid_6">
                     <div class="block-3">
                        <div class="count">4</div>
                        <div class="extra_wrapper">
                           <div class="text1"><a href="#">OxfordAQA International GCSE Year 1-12</a></div>
                           OxfordAQA allows schools to enhance their International GCSE, AS and A-level programmes with student-led, research-based projects. We have over 8 local subjects’ experienced tutors available for you everywhere for home tutoring. Hire best & professional tutors for your private lessons.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="grid_12">
                     <h3></h3>
                  </div>
                  <div class="grid_6">
                     <div class="block-3">
                        <div class="count">5</div>
                        <div class="extra_wrapper">
                           <div class="text1"><a href="#">Kenyan 8-4-4 Class 1-8; form 1to 4</a></div>
                           The 8–4–4 system of education, which has 8 years of primary education, 4 years of secondary education. We have all local subjects’ experienced tutors available for you everywhere for home tutoring On primary and secondary education level. Hire best & professional tutors for your private lessons.
                        </div>
                     </div>
                  </div>
                  <div class="grid_6">
                     <div class="block-3">
                        <div class="count">6</div>
                        <div class="extra_wrapper">
                           <div class="text1"><a href="#"> Kenyan 2-6-6-3 curriculum
                              Grade 1-12
                              </a>
                           </div>
                           The new system of education introduced in Kenya to replace the 8-4-4 system. We have all local subjects’ experienced tutors available for you everywhere for home tutoring On primary and secondary education level. Hire best & professional tutors for your private lessons.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="grid_12">
                     <h3></h3>
                  </div>
                  <div class="grid_6">
                     <div class="block-3">
                        <div class="count">7</div>
                        <div class="extra_wrapper">
                           <div class="text1"><a href="#">Adult Education</a></div>
                           Adult education, is a practice in which adults engage in systematic and sustained self-educating activities in order to gain new forms of knowledge, skills, attitudes, or values. Driven by what one needs or wants to learn, the available opportunities, and the manner in which one learns, adult learning is affected by demographics, globalization and technology. We have tutors who can help you in English learning - speaking, listening, writing and reading; we have also Swahili & mathematics tutors. Hire best & professional tutors for your private lessons.
                        </div>
                     </div>
                  </div>
                  <div class="grid_6">
                     <div class="block-3">
                        <div class="count">8</div>
                        <div class="extra_wrapper">
                           <div class="text1"><a href="#">The Test of English as a Foreign Language (TOEFL)</a></div>
                           Test of English as a Foreign Language (TOEFL) is a standardized test to measure the English language ability of non-native speakers wishing to enroll in English-speaking universities. The test is accepted by many English-speaking academic and professional institutions. TOEFL is one of the two major English-language tests in the world, the other being the IELTS.  We have experienced tutors available for you everywhere for home tutoring. Hire best & professional tutors for your private lessons.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </article>
      </section>
      <!--==============================
         footer
         =================================-->
      <footer id="footer">
         <div class="container">
            <div class="row">
               <div class="grid_12">
                  <div class="copyright pull-left"><span class="brand">HomeTutor</span> &copy; <span id="copyright-year"></span> | <a href="#">Privacy Policy</a>
                  </div>
                  <div class="sub-copy pull-right">Website designed by <a href="#" rel="nofollow">HomeTutor.co.ke</a></div>
               </div>
            </div>
         </div>
      </footer>
      <a href="#" id="toTop" class="fa fa-chevron-up"></a>
   </body>
</html>

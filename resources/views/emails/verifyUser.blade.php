@component('mail::message')
# Dear  {{  $user->name }} ,

 Welcome Email  click the link <a href="{{ route('verifyUser', $user->verifyUser->token) }}">Verify Your Registration </a>

 Login to proceed


Thanks,<br>
{{ config('app.name') }}
@endcomponent

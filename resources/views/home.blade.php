@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="tile">
                <div class="page-header">
                 <h2 class="mb-3 line-head" id="typography">Update Your Profile</h2>
                </div>
                <div class="row">
                        <div class="col-lg-6">
                        <form method="POST" class="form-horizontal" action="{{ route('postTutorProfile') }}">
                            @csrf
                        <div class="tile">
                                <div class="form-group{{ $errors->has('area_of_specialization') ? 'has-error' : ''}}">
                                        {!! Form::label('area_of_specialization', 'Area Of Specialization', ['class' => 'control-label']) !!}
                                        {!! Form::text('area_of_specialization', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                        {!! $errors->first('area_of_specialization', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group{{ $errors->has('academic_lavel') ? 'has-error' : ''}}">
                                        {!! Form::label('academic_lavel', 'Academic Lavel', ['class' => 'control-label']) !!}
                                        {!! Form::text('academic_lavel', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                        {!! $errors->first('academic_lavel', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group{{ $errors->has('certification') ? 'has-error' : ''}}">
                                        {!! Form::label('certification', 'Certification', ['class' => 'control-label']) !!}
                                        {!! Form::text('certification', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                        {!! $errors->first('certification', '<p class="help-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group{{ $errors->has('cert_upload') ? 'has-error' : ''}}">
                                        {!! Form::label('cert_upload', 'Cert Upload', ['class' => 'control-label']) !!}
                                        {!! Form::text('cert_upload', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                        {!! $errors->first('cert_upload', '<p class="help-block">:message</p>') !!}
                                    </div>
                         </div>
                         </div>
                         <div class="col-lg-4 offset-lg-1">
                               <div class="tile">
                                    <div class="form-group{{ $errors->has('years_of_experience') ? 'has-error' : ''}}">
                                            {!! Form::label('years_of_experience', 'Years Of Experience', ['class' => 'control-label']) !!}
                                            {!! Form::text('years_of_experience', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                            {!! $errors->first('years_of_experience', '<p class="help-block">:message</p>') !!}
                                        </div>
                                        <div class="form-group{{ $errors->has('subjects_of_specialization') ? 'has-error' : ''}}">
                                            {!! Form::label('subjects_of_specialization', 'Subjects Of Specialization', ['class' => 'control-label']) !!}
                                            {!! Form::text('subjects_of_specialization', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                            {!! $errors->first('subjects_of_specialization', '<p class="help-block">:message</p>') !!}
                                        </div>
                             </div>
                           </div>
                       </div>
                         <div class="tile-footer">
                                   <button class="btn btn-primary"  type="submit">Submit Updated Profile</button>
                         </div>
               </form>

              </div>
            </div>
          </div>
        </div>
    </div>


@endsection

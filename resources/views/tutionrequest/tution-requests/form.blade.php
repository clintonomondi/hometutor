<div class="form-group{{ $errors->has('start_tuition_date') ? 'has-error' : ''}}">
    {!! Form::label('start_tuition_date', 'Start Tuition Date', ['class' => 'control-label']) !!}
    {!! Form::input('datetime-local', 'start_tuition_date', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('start_tuition_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('end_tuition_date') ? 'has-error' : ''}}">
    {!! Form::label('end_tuition_date', 'End Tuition Date', ['class' => 'control-label']) !!}
    {!! Form::input('datetime-local', 'end_tuition_date', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('end_tuition_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('subject_category') ? 'has-error' : ''}}">
    {!! Form::label('subject_category', 'Subject Category', ['class' => 'control-label']) !!}
    {!! Form::select('subject_category',$categories, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('subject_category', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('select_subject') ? 'has-error' : ''}}">
    {!! Form::label('select_subject', 'Select Subject', ['class' => 'control-label']) !!}
    {!! Form::select('select_subject', $subjects, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('select_subject', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('total_tuition_cost') ? 'has-error' : ''}}">
    {!! Form::label('total_tuition_cost', 'Total Tuition Cost', ['class' => 'control-label']) !!}
    {!! Form::text('total_tuition_cost', null, ('required' == 'required') ? ['class' => 'form-control','required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('total_tuition_cost', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>

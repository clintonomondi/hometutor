@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Tutionrequests</div>
                    <div class="card-body">
                        <a href="{{ url('/tutionrequest/tution-requests/create') }}" class="btn btn-success btn-sm" title="Add New TutionRequest">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/tutionrequest/tution-requests', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Start Tuition Date</th><th>End Tuition Date</th><th>Subject Category</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($tutionrequests as $item)
                                    <tr role="row" class="odd">
                                    <td  class="sorting_1"> <button class="btn btn-outline-primary" type="button">{{ $item->status }}</button></t>
                                    <td>{{ $item->select_subject }}</td>
                                    <td>{{ number_format($item->total_tuition_cost) }}.00</td>
                                    <td>{{ $item->subject_category }}</td>
                                    <td>{{ $item->start_tuition_date }}</td>
                                    <td>{{ $item->end_tuition_date }}</td>
                                    <td>
                                            <a href="{{ url('/tutionrequest/tution-requests/' . $item->id) }}" title="View TutionRequest"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ route('moderatorAssignTutor', $item->id) }}" title="Edit TutionRequest"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            {!! Form::open([
                                                'method' => 'DELETE',
                                                'url' => ['/tutionrequest/tution-requests', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete TutionRequest',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $tutionrequests->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

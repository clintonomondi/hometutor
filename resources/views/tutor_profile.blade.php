@extends('admin.app')
@section('title') Dashboard @endsection
@section('content')
<div class="row user">
        <div class="col-md-3">
          <div class="tile p-0">
            <ul class="nav flex-column nav-tabs user-tabs">
              <li class="nav-item"><a class="nav-link active" href="#user-timeline" data-toggle="tab">Timeline</a></li>
              <li class="nav-item"><a class="nav-link" href="#user-settings" data-toggle="tab">Settings</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-9">
          <div class="tab-content">
            <div class="tab-pane active" id="user-timeline">
              <div class="timeline-post">

                <div class="post-content">
                                <div class="card">
                                        <div class="card-header">
                                            <button class="btn btn-secondary" type="button"><i class="fa fa-fw fa-lg fa-eye"></i>View Tutors Profile</button>

                                        </div>
                                        <div class="card-body">
                                            <strong> <p>  Learner Location </p></strong>
                                            <p>Learner Location</p>
                                        </div>
                                        <div class="card-footer">
                                            <strong>  <p>  Learner Location </p></strong>
                                            <p> Learner Location </p>
                                        </div>
                                        <div class="card-body">
                                                <strong> <p>  Learner Location </p></strong>
                                                <p>Learner Location</p>
                                            </div>
                                            <div class="card-footer">
                                                    <strong>  <p>  Learner Location </p></strong>
                                                    <p> Learner Location </p>
                                                </div>
                                    </div>
                                    {{-- <br>
                                    <div class="card">
                                        <div class="card-header">
                                            <button class="btn btn-secondary" type="button"><i class="fa fa-fw fa-lg fa-eye"></i>View Learners Profile</button>
                                        </div>
                                        <div class="card-body">
                                            <strong> <p>  Learner Name </p></strong>
                                            <p>Learner Location</p>
                                        </div>
                                        <div class="card-footer">
                                            <strong>  <p>  Learner Location </p></strong>
                                            <p>Learner Location </p>
                                        </div>
                                    </div> --}}
                </div>

              </div>

            </div>
            <div class="tab-pane fade" id="user-settings">
              <div class="tile user-settings">
                <h4 class="line-head">Settings</h4>
                <form>
                  <div class="row mb-4">
                    <div class="col-md-4">
                      <label>First Name</label>
                      <input class="form-control" type="text">
                    </div>
                    <div class="col-md-4">
                      <label>Last Name</label>
                      <input class="form-control" type="text">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8 mb-4">
                      <label>Email</label>
                      <input class="form-control" type="text">
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-8 mb-4">
                      <label>Mobile No</label>
                      <input class="form-control" type="text">
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-8 mb-4">
                      <label>Office Phone</label>
                      <input class="form-control" type="text">
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-8 mb-4">
                      <label>Home Phone</label>
                      <input class="form-control" type="text">
                    </div>
                  </div>
                  <div class="row mb-10">
                    <div class="col-md-12">
                      <button class="btn btn-primary" type="button"><i class="fa fa-fw fa-lg fa-check-circle"></i> Save</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
 @endsection

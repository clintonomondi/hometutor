<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\TutorProfile;
use Faker\Generator as Faker;

$factory->define(TutorProfile::class, function (Faker $faker) {
    return [

        'area_of_specialization' => 'Certificate',
        'academic_level' => 'Botanic Science',
        'certification' => 'Degree', // password
        'cert_upload' => 'test upload',
        'years_of_experience' => '3',
        'subjects_of_specialization' => 'English',
        'user_id' => User::all()->random()->id,
        'status' => 1,


    ];
});

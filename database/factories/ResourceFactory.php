<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\User;
use App\Resource;
use Faker\Generator as Faker;

$factory->define(Resource::class, function (Faker $faker) {

    return [
        'category_id' => Category::all()->random()->id, // password
        'title' => 'Test title', // password
        'body' => 'Test Body',
        'user_id' => User::all()->random()->id,
    ];
});

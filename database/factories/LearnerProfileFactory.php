<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\LearnerProfile;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Auth;

$factory->define(LearnerProfile::class, function (Faker $faker) {
    return [
        'gender' => 'male',
        'education_system' => 'Degree', // password
        'learning_level' => 'test upload',
        'user_id' => User::all()->random()->id,
    ];
});

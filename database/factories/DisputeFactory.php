<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Dispute;
use App\TutionRequest;
use Faker\Generator as Faker;

$factory->define(Dispute::class, function (Faker $faker) {
    return [

        'title' => $faker->sentence(10),
        'body' => $faker->sentence(50),
        'user_id' => User::all()->random()->id,
        'tution_id' => TutionRequest::all()->random()->id


    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Model;
use App\Tutor;
use App\TutionRequest;
use Faker\Generator as Faker;

$factory->define(TutionRequest::class, function (Faker $faker) {
    return [
        'start_tuition_date' => $faker->dateTimeAD($max = 'now', $timezone = null),
        'end_tuition_date' => $faker->dateTimeAD($max = 'now', $timezone = null),
        'subject_category' => 'Test Body',
        'select_subject' => 'Test Body',
        'total_tuition_cost' => 40,
        'status' => 'test',
        'user_id' => User::all()->random()->id,
        // 'tutor_id' => Tutor::all()->random()->id,
    ];
});

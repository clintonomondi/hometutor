<?php

use App\TutorProfile;
use Illuminate\Database\Seeder;

class TutorProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(TutorProfile::class, 50)->create();
    }
}

<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $clientR = Category::create([
            'group' => 'A',
            'group_name' => 'Languages'
        ]);

        $consultantR = Category::create([
            'group' => 'B',
            'group_name' => 'Humanities and Social Sciences'
        ]);

        $moderatort = Category::create([
            'group' => 'C',
            'group_name' => 'Mathematics & Sciences'
        ]);

        $moderatork = Category::create([
            'group' => 'D',
            'group_name' => 'Creative, Technical and Vocational'
        ]);

       
    }
}

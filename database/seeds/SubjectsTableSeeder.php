<?php

use Illuminate\Database\Seeder;
use App\Subject;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superR = Subject::create([
            'category_id' => '1',
            'subject_code' => '001',
            'subject_name' => 'Accounting '
        ]);


        $clientR = Subject::create([
            'category_id' => '2',
            'subject_code' => '002',
            'subject_name' => 'Art & Design'
        ]);

        $consultantR = Subject::create([
            'category_id' => '3',
            'subject_code' => '003',
            'subject_name' => 'Art, Design and Media'
        ]);

        $moderatort = Subject::create([
            'category_id' => '4',
            'subject_code' => '004',
            'subject_name' => 'Business Studies '
        ]);

        $moderatork = Subject::create([
            'category_id' => '4',
            'subject_code' => '005',
            'subject_name' => 'Child Development'
        ]);

        $moderatorRd = Subject::create([
            'category_id' => '3',
            'subject_code' => '006',
            'subject_name' => 'Computer Science'
        ]);

        $moderatorRb1 = Subject::create([
            'category_id' => '3',
            'subject_code' => '007',
            'subject_name' => 'Drama'
        ]);
        $moderatorRb2 = Subject::create([
            'category_id' => '1',
            'subject_code' => '008',
            'subject_name' => 'Drama, Theatre Studies and Performing Arts'
        ]);


        $moderatorR3 = Subject::create([
            'category_id' => '2',
            'subject_code' => '009',
            'subject_name' => 'Enterprise'
        ]);
        $moderatorRb4 = Subject::create([
            'category_id' => '1',
            'subject_code' => '010',
            'subject_name' => 'Food & Nutrition '
        ]);

        $moderatorRb5 = Subject::create([
            'category_id' => '2',
            'subject_code' => '011',
            'subject_name' => 'Food Manufacturing'
        ]);

        $moderatorRb6 = Subject::create([
            'category_id' => '3',
            'subject_code' => '012',
            'subject_name' => 'Finance and Accounting'
        ]);

        $moderatorRb7 = Subject::create([
            'category_id' => '1',
            'subject_code' => '013',
            'subject_name' => 'Information & Communication Technology'
        ]);

        $moderatorRb8 = Subject::create([
            'category_id' => '2',
            'subject_code' => '014',
            'subject_name' => 'Music'
        ]);

        $moderatorRb9 = Subject::create([
            'category_id' => '3',
            'subject_code' => '015',
            'subject_name' => 'Physical Education'
        ]);

        $moderatorRb10 = Subject::create([
            'category_id' => '4',
            'subject_code' => '016',
            'subject_name' => 'Construction, Planning and the Built Environment'
        ]);

        $moderatorRb11 = Subject::create([
            'category_id' => '1',
            'subject_code' => '017',
            'subject_name' => 'Travel & Tourism'
        ]);
        $moderatorRb12 = Subject::create([
            'category_id' => '2',
            'subject_code' => '018',
            'subject_name' => 'First Aid'
        ]);
        $moderatorRb13 = Subject::create([
            'category_id' => '4',
            'subject_code' => '019',
            'subject_name' => 'Beauty Therapy and Hairdressing'
        ]);
        $moderatorRb14 = Subject::create([
            'category_id' => '3',
            'subject_code' => '020',
            'subject_name' => 'Animal Care and Veterinary Science'
        ]);

        $moderatorRb15 = Subject::create([
            'category_id' => '1',
            'subject_code' => '021',
            'subject_name' => 'English'
        ]);
        $moderatorRb16 = Subject::create([
            'category_id' => '3',
            'subject_code' => '022',
            'subject_name' => 'English - First Language'
        ]);
        $moderatorRb17 = Subject::create([
            'category_id' => '4',
            'subject_code' => '023',
            'subject_name' => 'English – Literature'
        ]);
        $moderatorRb18 = Subject::create([
            'category_id' => '2',
            'subject_code' => '024',
            'subject_name' => 'English as a Second Language'
        ]);
        $moderatorRb19 = Subject::create([
            'category_id' => '1',
            'subject_code' => '025',
            'subject_name' => 'Swahili'
        ]);
        $moderatorRb20 = Subject::create([
            'category_id' => '4',
            'subject_code' => '026',
            'subject_name' => 'World Literature'
        ]);

        $moderatorRb21 = Subject::create([
            'category_id' => '3',
            'subject_code' => '027',
            'subject_name' => 'Arabic - First Language'
        ]);

        $moderatorRb22 = Subject::create([
            'category_id' => '2',
            'subject_code' => '028',
            'subject_name' => 'Chinese - First Language'
        ]);
        $moderatorRb23 = Subject::create([
            'category_id' => '1',
            'subject_code' => '029',
            'subject_name' => 'Chinese - Second Language'
        ]);
        $moderatorRb24 = Subject::create([
            'category_id' => '4',
            'subject_code' => '030',
            'subject_name' => 'French - First Language'
        ]);
        $moderatorRb25 = Subject::create([
            'category_id' => '2',
            'subject_code' => '031',
            'subject_name' => 'French - Foreign Language'
        ]);

        $moderatorRb26 = Subject::create([
            'category_id' => '3',
            'subject_code' => '032',
            'subject_name' => 'French'
        ]);
        $moderatorR27 = Subject::create([
            'category_id' => '2',
            'subject_code' => '033',
            'subject_name' => 'Germany'
        ]);
        $moderatorRb28 = Subject::create([
            'category_id' => '1',
            'subject_code' => '034',
            'subject_name' => 'German - First Language '
        ]);
        $moderatorRb29 = Subject::create([
            'category_id' => '4',
            'subject_code' => '035',
            'subject_name' => 'German - Foreign Language'
        ]);
        $moderatorRb30 = Subject::create([
            'category_id' => '3',
            'subject_code' => '036',
            'subject_name' => 'Spanish - First Language'
        ]);
        $moderatorRb40 = Subject::create([
            'category_id' => '2',
            'subject_code' => '037',
            'subject_name' => 'Spanish - Foreign Language'
        ]);

        //////////////////////////////////////////////////////////
        $moderatorRb41 = Subject::create([
            'category_id' => '4',
            'subject_code' => '038',
            'subject_name' => 'The International English Language Testing System (IELTS)'
        ]);

        $moderatorRb42 = Subject::create([
            'category_id' => '4',
            'subject_code' => '039',
            'subject_name' => 'The Test of English as a Foreign Language (TOEFL)'
        ]);

        $moderatorRb43 = Subject::create([
            'category_id' => '1',
            'subject_code' => '040',
            'subject_name' => 'Development Studies'
        ]);
        $moderatorRb44 = Subject::create([
            'category_id' => '2',
            'subject_code' => '041',
            'subject_name' => 'Health and Social Care'
        ]);
        $moderatorRb45 = Subject::create([
            'category_id' => '3',
            'subject_code' => '042',
            'subject_name' => 'Business, Administration and Law'
        ]);
        $moderatorRb46 = Subject::create([
            'category_id' => '2',
            'subject_code' => '043',
            'subject_name' => 'Economics'
        ]);

        $moderatorRb47 = Subject::create([
            'category_id' => '3',
            'subject_code' => '044',
            'subject_name' => 'Geography'
        ]);
        $moderatorR48 = Subject::create([
            'category_id' => '2',
            'subject_code' => '045',
            'subject_name' => 'General Studies'
        ]);
        $moderatorRb49 = Subject::create([
            'category_id' => '1',
            'subject_code' => '046',
            'subject_name' => 'Global Perspectives'
        ]);
        $moderatorRb50 = Subject::create([
            'category_id' => '4',
            'subject_code' => '047',
            'subject_name' => 'History'
        ]);
        $moderatorRb51 = Subject::create([
            'category_id' => '2',
            'subject_code' => '048',
            'subject_name' => 'India Studies'
        ]);
        $moderatorRb52 = Subject::create([
            'category_id' => '2',
            'subject_code' => '049',
            'subject_name' => 'Islamiyat'
        ]);
        //////////////////////////////////////
        $moderatorRb53 = Subject::create([
            'category_id' => '1',
            'subject_code' => '050',
            'subject_name' => 'Religious Studies'
        ]);
        $moderatorRb54 = Subject::create([
            'category_id' => '4',
            'subject_code' => '051',
            'subject_name' => 'Sociology'
        ]);
        $moderatorRb55 = Subject::create([
            'category_id' => '3',
            'subject_code' => '052',
            'subject_name' => 'Mathematics'
        ]);

        $moderatorRb56 = Subject::create([
            'category_id' => '1',
            'subject_code' => '053',
            'subject_name' => 'Maths and Statistics'
        ]);
        $moderatorR57 = Subject::create([
            'category_id' => '3',
            'subject_code' => '054',
            'subject_name' => 'Agriculture'
        ]);
        $moderatorRb59 = Subject::create([
            'category_id' => '1',
            'subject_code' => '055',
            'subject_name' => 'Biology'
        ]);
        $moderatorRb60 = Subject::create([
            'category_id' => '3',
            'subject_code' => '056',
            'subject_name' => 'Chemistry'
        ]);
        $moderatorRb61 = Subject::create([
            'category_id' => '4',
            'subject_code' => '057',
            'subject_name' => 'Environmental Management'
        ]);
        $moderatorRb62 = Subject::create([
            'category_id' => '2',
            'subject_code' => '058',
            'subject_name' => 'Physical Education'
        ]);

        /////
        $moderatorR63 = Subject::create([
            'category_id' => '3',
            'subject_code' => '059',
            'subject_name' => 'Physical Science'
        ]);
        $moderatorRb64 = Subject::create([
            'category_id' => '3',
            'subject_code' => '060',
            'subject_name' => 'Physics'
        ]);
        $moderatorRb65 = Subject::create([
            'category_id' => '4',
            'subject_code' => '061',
            'subject_name' => 'Engineering'
        ]);
        $moderatorRb66 = Subject::create([
            'category_id' => '3',
            'subject_code' => '062',
            'subject_name' => 'Science – Combined'
        ]);
        $moderatorRb67 = Subject::create([
            'category_id' => '2',
            'subject_code' => '063',
            'subject_name' => 'Animal Care and Veterinary Science'
        ]);
        $moderatorRb68 = Subject::create([
            'category_id' => '1',
            'subject_code' => '064',
            'subject_name' => 'Medicine and Dentistry'
        ]);
    }
}

<?php

use App\Dispute;
use Illuminate\Database\Seeder;

class DisputesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Dispute::class, 20)->create();
    }
}

<?php

use App\TutionRequest;
use Illuminate\Database\Seeder;

class TutionRequestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(TutionRequest::class, 20)->create();
    }
}

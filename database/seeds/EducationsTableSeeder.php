<?php

use App\Education;
use Illuminate\Database\Seeder;

class EducationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clientR = Education::create([
            'group' => 'Year 1 -12',
            'education_name' => 'Cambridge IGCSE'
        ]);

        $consultantR = Education::create([
            'group' => 'Year 1 -12',
            'education_name' => 'Edexcel International GCSE'
        ]);

        $moderatort = Education::create([
            'group' => 'Year 1 -12',
            'education_name' => 'Oxford AQA International GCSE'
        ]);

        $moderatork1 = Education::create([
            'group' => 'Year 1 -12',
            'education_name' => 'Kenyan 8-4-4 curriculum'
        ]);
        $moderatork2 = Education::create([
            'group' => 'Year 1 -12',
            'education_name' => 'Kenyan 2-6-6-3 curriculum'
        ]);
        $moderatork3 = Education::create([
            'group' => 'Year 1 -12',
            'education_name' => 'The International English Language Testing System (IELTS)'
        ]);
        $moderatork4 = Education::create([
            'group' => 'Year 1 -12',
            'education_name' => '	The Test of English as a Foreign Language (TOEFL)'
        ]);
        $moderatork4 = Education::create([
            'group' => 'Year 1 -12',
            'education_name' => '8.	Adult Education'
        ]);
    }
}

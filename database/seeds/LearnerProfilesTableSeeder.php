<?php

use App\LearnerProfile;
use Illuminate\Database\Seeder;

class LearnerProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(LearnerProfile::class, 50)->create();
    }
}

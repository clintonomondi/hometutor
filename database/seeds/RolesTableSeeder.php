<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $superR = Role::create([
            'name' => 'Administrator',
            'label' => 'Administrator'
        ]);


        $clientR = Role::create([
            'name' => 'Learner',
            'label' => 'Learner'
        ]);

        $consultantR = Role::create([
            'name' => 'Tutor',
            'label' => 'Tutor'
        ]);

        $moderatorR = Role::create([
            'name' => 'Moderator',
            'label' => 'moderator'
        ]);
    }
}

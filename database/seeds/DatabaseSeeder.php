<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(TutionRequestsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(DisputesTableSeeder::class);
        $this->call(SubjectsTableSeeder::class);
        $this->call(ResourcesTableSeeder::class);
        $this->call(EducationsTableSeeder::class);
        $this->call(TutorProfilesTableSeeder::class);
        $this->call(LearnerProfilesTableSeeder::class);
    }
}

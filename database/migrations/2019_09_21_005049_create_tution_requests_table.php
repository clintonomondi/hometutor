<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutionRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tution_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->dateTime('start_tuition_date')->nullable();
            $table->dateTime('end_tuition_date')->nullable();
            $table->string('subject_category')->nullable();
            $table->string('select_subject')->nullable();
            $table->string('status')->nullable();
            $table->string('total_tuition_cost')->nullable();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }


    public function down()
    {
        Schema::drop('tution_requests');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('status')->nullable();
            $table->string('area_of_specialization')->nullable();
            $table->string('academic_level')->nullable();
            $table->string('certification')->nullable();
            $table->string('cert_upload')->nullable();
            $table->string('years_of_experience')->nullable();
            $table->string('subjects_of_specialization')->nullable();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tutor_profiles');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLearnerProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('learner_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
           
            $table->string('status')->nullable();
            $table->string('gender')->nullable();
            $table->string('education_system')->nullable();
            $table->string('learning_level')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('learner_profiles');
    }
}

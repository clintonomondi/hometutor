<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisputesTable extends Migration
{
    public function up()
    {

        Schema::create('disputes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('body');
            $table->string('title');

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('tution_id');
            $table->foreign('tution_id')->references('id')->on('tution_requests')->onDelete('cascade');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('disputes');
    }
}

<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
});

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser')->name('verifyUser');
Route::post('/get_c2bConfirmation', 'PaymentsController@c2bConfirmation')->name('getC2BConfirmation');
Route::post('/get_c2KConfrmation',  'PaymentsController@c2KConfrmation')->name('getC2KConfrmation');

Route::get('/our_curriculum', 'CurriculumController@index')->name('ourCurriculum');
Route::get('/register_learner', 'Auth\RegisterController@getLearner')->name('getLearner');
Route::post('/register_learner', 'Auth\RegisterController@register')->name('registerLearner');


Auth::routes();


Route::group(['middleware' => ['auth']], function () {

    Route::resource('tutionrequest/tution-requests', 'TutionRequest\\TutionRequestsController');
    Route::post('/post_sumulateC2B', 'PaymentsController@sumulateC2B')->name('postSumulateC2B');
    Route::get('/view_payments', 'PaymentsController@view_payments')->name('viewPayments');


    Route::get('/view_tutor_profile', 'ProfileController@view_tutor_profile')->name('viewTutorProfile');
    Route::get('/view_learner_profile', 'ProfileController@view_learner_profile')->name('viewLearnerProfile');

    Route::get('/moderator_test/{id}', 'PaymentsController@showIndex')->name('showIndex');
    Route::get('/home', 'HomeController@index')->name('home');

    // Route::post('/get_c2KConfrmation',  'PaymentsController@c2KConfrmation')->name('getC2KConfrmation');
    // Route::post('/get_c2bConfirmation', 'PaymentsController@c2bConfirmation')->name('getC2BConfirmation');

    Route::get('referal/{referal_code}', 'ReferalController@referClient')->name('referClient');
    Route::resource('referal/referals', 'ReferalController');

    Route::get('/tutor', 'TutorController@index')->name('tutorIndex');
    Route::get('/tutor_create', 'TutorController@create')->name('tutorCreate');
    Route::post('/tutor', 'TutorController@store')->name('tutorPost');

    /*to add the request id*/
    Route::get('/review_completed_request/{id}/review', 'LearnerController@review_completed_request')->name('reviewCompletedRequests');
    /*to add the request dispute completed id*/
    // Route::get('/dispute_completed_request/{id}', 'LearnerController@dispute_completed_request')->name('disputeCompletedRequests');

    Route::get('/learner', 'LearnerController@index')->name('learnerIndex');
    Route::get('/learner_create', 'LearnerController@create')->name('learnerCreate');
    Route::post('/learner', 'LearnerController@store')->name('learnerPost');
    Route::post('/post_dispute/{id}', 'LearnerController@post_dispute')->name('postDispute');


    Route::get('/moderator', 'ModeratorController@index')->name('moderatorIndex');
    Route::get('/moderator_listDisputes', 'ModeratorController@listDisputes')->name('listDisputes');
    Route::get('/moderator_show_request/{id}', 'ModeratorController@showRequest')->name('moderatorShowRequest');
    Route::get('/moderator_assign_tutor/{id}', 'ModeratorController@assignTutor')->name('moderatorAssignTutor');
    Route::get('/moderator_create', 'ModeratorController@create')->name('moderatorCreate');
    Route::post('/moderator', 'ModeratorController@store')->name('moderatorPost');



    // Route::get('/admin_index', 'AdminController@index')->name('admin_Index');
    // Route::get('/admin_show/{id}', 'AdminController@showRequest')->name('admin_ShowRequest');
    // Route::get('/admin_assign/{id}', 'AdminController@assignTutor')->name('admin_AssignTutor');
    // Route::get('/admin_create', 'AdminController@create')->name('admin_Create');
    // Route::post('/admin_store', 'AdminController@store')->name('moderatorPost');


    Route::post('/c2b_resutlt', 'PaymentsController@c2bConfirmation');
    Route::get('/create_an_order', 'OrderController@create_order')->name('createAnOrder');
    Route::post('/post_an_order', 'OrderController@post_an_order')->name('postAnOrder');
    Route::put('/update_an_order', 'OrderController@update_an_order')->name(' updateAnOrder');

    Route::get('/resource', 'ResourceController@index')->name('resourceIndex');
    Route::get('/create_resource', 'ResourceController@create_resource')->name('createResource');
    Route::post('/post_resource', 'ResourceController@post_resource')->name('postResource');
    Route::get('/show_resource', 'ResourceController@show_resource')->name('showResource');


    Route::post('/post_tutor-profiles', 'TutorProfile\\TutorProfilesController@postTutorProfile')->name('postTutorProfile');
    Route::post('/post_learner-profiles', 'LearnerProfile\\LearnerProfilesController@postLearnerProfile')->name('postLearnerProfile');
    Route::post('/resource', 'ResourceController@store')->name('resourcePost');

    Route::get('/admin/dashboard_tutors_view', 'DashboardController@getTutors')->name('getTutorMermbers');
    Route::get('/admin/dashboard_learners_view', 'DashboardController@getLearners')->name('getLearnerMermbers');
    Route::get('/admin/dashboard_tution_requests', 'DashboardController@getTutionRequests')->name('dashboardTutionRequests');

    Route::resource('tutorProfile/tutor-profiles', 'TutorProfile\\TutorProfilesController');
    Route::resource('learnerProfile/learner-profiles', 'LearnerProfile\\LearnerProfilesController');

    Route::get('admin', 'Admin\AdminController@index');
    Route::resource('admin/roles', 'Admin\RolesController');
    Route::resource('admin/permissions', 'Admin\PermissionsController');
    Route::resource('admin/users', 'Admin\UsersController');
    Route::resource('admin/pages', 'Admin\PagesController');
    Route::resource('admin/activitylogs', 'Admin\ActivityLogsController')->only(['index', 'show', 'destroy']);

    Route::resource('admin/settings', 'Admin\SettingsController');
    Route::get('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
    Route::post('admin/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::post('/profile/update', 'ProfileController@updateProfile')->name('profile.update');
});

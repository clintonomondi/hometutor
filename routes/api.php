<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::post('/get_c2bConfirmation', 'PaymentsController@c2bConfirmation')->name('getC2BConfirmation');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
